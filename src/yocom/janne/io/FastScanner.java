package yocom.janne.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class FastScanner implements AutoCloseable
{
	BufferedReader br;
	StringTokenizer stok;

	public FastScanner(InputStream is)
	{
		br = new BufferedReader(new InputStreamReader(is));
	}

	public String nextToken() throws IOException
	{
		while (stok == null || !stok.hasMoreTokens())
		{
			String s = br.readLine();
			if (s == null)
			{
				return null;
			}
			stok = new StringTokenizer(s);
		}
		return stok.nextToken();
	}

	public void mark(int readAheadLimit) throws IOException
	{
		br.mark(readAheadLimit);
	}

	public String next() throws IOException
	{
		return nextToken();
	}

	public int nextInt() throws IOException
	{
		return Integer.parseInt(nextToken());
	}

	public long nextLong() throws IOException
	{
		return Long.parseLong(nextToken());
	}

	public double nextDouble() throws IOException
	{
		return Double.parseDouble(nextToken());
	}

	public float nextFloat() throws IOException
	{
		return Float.parseFloat(nextToken());
	}

	public char nextChar() throws IOException
	{
		return (char) (br.read());
	}

	public String nextLine() throws IOException
	{
		return br.readLine();
	}

	public void reset() throws IOException
	{
		br.reset();
	}

	@Override
	public void close() throws Exception
	{
		br.close();
		stok = null;
	}
}
