package yocom.janne.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.BaseNeuron;
import yocom.janne.base.NeuralNetProgram;
import yocom.janne.base.NeuralNetExecutor;
import yocom.janne.traintoolkit.Epoch;
import yocom.janne.traintoolkit.Set;
import yocom.janne.types.LogType;

public class Log
{
	private static final Logger info;
	private static final Logger err;
	private static final Logger debug;
	// private static final DateFormat dateFormat = new
	// SimpleDateFormat("yyyy.MM.dd_HH.mm.ss.SSS");
	// private static FileOutputStream fos;
	// private static Path ptlf;
	// private static boolean initedFH = false;
	private static boolean infoEnabled = true;
	private static boolean debugEnabled = true;
	private static boolean errorEnabled = true;
	private static boolean logEnabled = true;

	static
	{
		System.setProperty("log4j.configurationFile", Log.class.getResource("/res/log4j2.xml").getPath());

		info = LogManager.getLogger("JANNE-INFO");
		err = LogManager.getLogger("JANNE-ERROR");
		debug = LogManager.getLogger("JANNE-DEBUG");

		// System.setOut(IoBuilder.forLogger(info).setLevel(Level.INFO).buildPrintStream());
	}

	public static void enableLogging()
	{
		logEnabled = true;
	}

	public static void disableLogging()
	{
		logEnabled = false;
	}

	public static void enableInfoLogging()
	{
		infoEnabled = true;
	}

	public static void disableInfoLogging()
	{
		infoEnabled = false;
	}

	public static void enableDebugLogging()
	{
		debugEnabled = true;
	}

	public static void disableDebugLogging()
	{
		debugEnabled = false;
	}

	public static void enableErrorLogging()
	{
		errorEnabled = true;
	}

	public static void disableErrorLogging()
	{
		errorEnabled = false;
	}

	public static <T> void printArray(String splitter, ArrayList<T> array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.size(); i++)
		{
			message += array.get(i).toString() + splitter;
		}
		;

		debug(message + "");
	}

	public static void printArray(String splitter, boolean... array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.length; i++)
		{
			message += array[i] + splitter;
		}
		;

		debug(message + "");
	}

	public static void printArray(String splitter, double... array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.length; i++)
		{
			message += array[i] + splitter;
		}
		;

		debug(message + "");
	}

	public static void printArray(String splitter, float... array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.length; i++)
		{
			message += array[i] + splitter;
		}
		;

		debug(message + "");
	}

	public static void printArray(String splitter, byte... array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.length; i++)
		{
			message += array[i] + splitter;
		}
		;

		debug(message + "");
	}

	public static void printArray(String splitter, short... array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.length; i++)
		{
			message += array[i] + splitter;
		}
		;

		debug(message + "");
	}

	public static void printArray(String splitter, int... array)
	{
		if (array == null)
		{
			error("Null array!");
			return;
		}
		if (splitter == null)
		{
			error("Null splitter!");
			return;
		}
		String message = "";
		for (int i = 0; i < array.length; i++)
		{
			message += array[i] + splitter;
		}
		;

		debug(message + "");
	}

	public static void info(boolean message)
	{
		info(message + "");
	}

	public static void info(double message)
	{
		info(message + "");
	}

	public static void info(float message)
	{
		info(message + "");
	}

	public static void info(byte message)
	{
		info(message + "");
	}

	public static void info(short message)
	{
		info(message + "");
	}

	public static void info(int message)
	{
		info(message + "");
	}

	public static void info(String message)
	{
		log(LogType.INFO, message, null);
	}

	public static void debug(boolean message)
	{
		debug(message + "");
	}

	public static void debug(double message)
	{
		debug(message + "");
	}

	public static void debug(float message)
	{
		debug(message + "");
	}

	public static void debug(byte message)
	{
		debug(message + "");
	}

	public static void debug(short message)
	{
		debug(message + "");
	}

	public static void debug(int message)
	{
		debug(message + "");
	}

	public static void debug(String message)
	{
		log(LogType.DEBUG, message, null);
	}

	public static void error(boolean message)
	{
		error(message + "");
	}

	public static void error(double message)
	{
		error(message + "");
	}

	public static void error(float message)
	{
		error(message + "");
	}

	public static void error(byte message)
	{
		error(message + "");
	}

	public static void error(short message)
	{
		error(message + "");
	}

	public static void error(int message)
	{
		error(message + "");
	}

	public static void error(String message)
	{
		log(LogType.ERROR, message, null);
	}

	public static void error(String message, Throwable e)
	{
		log(LogType.ERROR, message, e);
	}

	public static void log(LogType type, String message, Throwable t)
	{
		if (!logEnabled)
			return;
		switch (type)
		{
			case DEBUG:
				if (!debugEnabled)
					return;
				debug.debug(message);
				break;
			case ERROR:
				if (!errorEnabled)
					return;
				err.warn(message, t);
				break;
			case INFO:
				if (!infoEnabled)
					return;
				info.info(message);
				break;
		}
	}

	public static void createTrainingReport(BaseNeuralNetwork net, NeuralNetProgram prog, Epoch e, double e2, double a,
			double iterations)
	{
		File jarFolder = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath()).getParentFile();
		File logFile = new File(jarFolder.getPath() + "/trainingReport.log");
		try
		{
			FileWriter fw = new FileWriter(logFile);
			fw.write("Neuron : input : output : delta" + '\n');
			for (int i = 0; i < net.numLayers(); i++)
				for (int j = 0; j < net.layerLength(i); j++)
				{
					BaseNeuron t = net.getLayer(i).get(j);
					fw.write(i + " " + j + " : " + t.obtained() + " : " + t.created() + " : " + t.delta() + '\n');
					for (BaseNeuron g : t.getters())
						fw.write("(" + i + " " + j + ") : " + g.getLayerID() + " " + g.getLayerNID() + ") : "
								+ net.getWeightSynapse(t, g) + '\n');
				}
			fw.write("Set inputs : Set outputs" + '\n');
			for (Set s : e)
			{
				fw.write(Arrays.toString(s.getInputsD()) + " : " + Arrays.toString(s.getOutputsD()) + '\n');
				fw.write(Arrays.toString(NeuralNetExecutor.run(net, prog, s.getInputs())) + '\n');
			}

			fw.flush();
			fw.close();
		} catch (IOException e1)
		{
			Log.error("Error during creating training report!");
		}
	}

	public static void createTrainingReport(BaseNeuralNetwork net, NeuralNetProgram prog, File tsf, double e2, double a,
			double iterations)
	{
		File jarFolder = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath()).getParentFile();
		File logFile = new File(jarFolder.getPath() + "/trainingReport.log");
		try
		{
			FileWriter fw = new FileWriter(logFile);
			fw.write("Neuron : input : output : delta" + '\n');
			for (int i = 0; i < net.numLayers(); i++)
				for (int j = 0; j < net.layerLength(i); j++)
				{
					BaseNeuron t = net.getLayer(i).get(j);
					fw.write(i + " " + j + " : " + t.obtained() + " : " + t.created() + " : " + t.delta() + '\n');
					for (BaseNeuron g : t.getters())
						fw.write("(" + i + " " + j + ") : " + g.getLayerID() + " " + g.getLayerNID() + ") : "
								+ net.getWeightSynapse(t, g) + '\n');
				}
			fw.write("Trainset file path: " + tsf.getPath());

			fw.flush();
			fw.close();
		} catch (IOException e1)
		{
			Log.error("Error during creating training report!");
		}

	}

}
