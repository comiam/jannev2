package yocom.janne.network;

import java.io.FileWriter;
import java.util.ArrayList;

import com.google.common.collect.Table.Cell;

import yocom.janne.base.BaseLayer;
import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.BaseNeuron;
import yocom.janne.io.FastWriter;
import yocom.janne.io.Log;
import yocom.janne.neurons.DefNeuron;
import yocom.janne.types.NeuralGen;

public class DefNeuralNetwork extends BaseNeuralNetwork
{
	private static final long serialVersionUID = -5417276977381476311L;
	
	public DefNeuralNetwork()
	{
		init();
	}

	@Override
	public void init()
	{
		networkGen = NeuralGen.Default;
	}
	
	@Override
	public boolean save(String path)
	{
		String tab = "   ";
		try (FastWriter printer = new FastWriter(new FileWriter(path.endsWith(".ann") ? path : path + ".ann")))
		{
			printer.writeln("JANNE neural network");
			printer.writeln(".head");
			printer.writeln(tab + "current version: " + currentVersion);
			printer.writeln(tab + "neuralGen: " + networkGen.name());
			printer.writeln(tab + "neuralType: " + networkType.name());
			printer.writeln(tab + "error: " + currentError);
			printer.writeln(tab + "layers: " + numLayers());
			printer.writeln(tab + "synapses: " + weights.size());
			printer.writeln(".end-head");
			printer.writeln(".body");
			printer.writeln(tab + "layers:");
			BaseLayer bl;
			ArrayList<BaseNeuron> tmp;
			for (int i = 0; i < numLayers(); i++)
			{
				bl = getLayer(i);
				printer.writeln(tab + tab + "layer " + i);
				printer.writeln(tab + tab + tab + bl.size());
				printer.writeln(tab + tab + tab + bl.getType());
				printer.writeln(tab + tab + tab + "neurons:");
				for (int j = 0; j < bl.size(); j++)
				{
					printer.writeln(tab + tab + tab + tab + "neuron " + j);
					printer.writeln(tab + tab + tab + tab + tab + 6);
					printer.writeln(tab + tab + tab + tab + tab + "activationType: " + bl.get(j).getAType().name());
					printer.writeln(tab + tab + tab + tab + tab + "neuronType: " + bl.get(j).getNType().name());
					printer.writeln(tab + tab + tab + tab + tab + "bias: " + ((DefNeuron)bl.get(j)).bias());
					printer.writeln(tab + tab + tab + tab + tab + "steepness: " + ((DefNeuron)bl.get(j)).steepness());
					printer.write(tab + tab + tab + tab + tab + "getters: ");
					tmp = bl.get(j).getters();
					for (int j2 = 0; j2 < tmp.size(); j2++)
					{
						printer.write(tmp.get(j2).getLayerID() + " " + tmp.get(j2).getLayerNID() + " ");
					}
					printer.writeln("|");
					printer.write(tab + tab + tab + tab + tab + "setters: ");
					tmp = bl.get(j).setters();
					for (int j2 = 0; j2 < tmp.size(); j2++)
					{
						printer.write(tmp.get(j2).getLayerID() + " " +tmp.get(j2).getLayerNID() + " ");
					}
					printer.writeln("|");
					printer.writeln(tab + tab + tab + tab + "endData.");
				}
			}
			printer.writeln(tab + "synapses:");
			for (Cell<BaseNeuron, BaseNeuron, Double> hash : weights.cellSet())
			{
				printer.writeln(tab + tab + hash.getRowKey().getLayerID() + " " + hash.getRowKey().getLayerNID() + " "  + hash.getColumnKey().getLayerID() + " " + hash.getColumnKey().getLayerNID() + " " + hash.getValue());
			}
			printer.writeln(".end-body");
			return true;
		}catch(Throwable e)
		{
			Log.error("Error during writing of the neural network " + networkType + " to a file!");
			return false;
		}
	}
}
