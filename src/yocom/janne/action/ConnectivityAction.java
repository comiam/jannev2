package yocom.janne.action;

import yocom.janne.base.BaseLayer;
import yocom.janne.base.BaseNeuralNetwork;

@FunctionalInterface
public interface ConnectivityAction
{
	public void connect(BaseNeuralNetwork net, BaseLayer n0, BaseLayer n1);
}
