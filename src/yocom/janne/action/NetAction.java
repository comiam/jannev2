package yocom.janne.action;

import yocom.janne.types.NetProgramActions;

public class NetAction
{
	private NetProgramActions action;
	private Runnable customAction;
	private int lID;
	
	public NetAction(NetProgramActions action, int lID)
	{
		setAll(action, lID);
	}
	
	public NetAction(Runnable customAction, int lID)
	{
		setAll(customAction, lID);
	}
	
	public void setAll(NetProgramActions action, int lID)
	{
		this.action = action;
		this.lID = lID;
		this.customAction = null;
	}
	
	public void setAll(Runnable customAction, int lID)
	{
		this.action = null;
		this.lID = lID;
		this.customAction = customAction;
	}
	
	public void setlID(int lID)
	{
		this.lID = lID;
	}
	
	public void setCustomAction(Runnable action)
	{
		this.action = null;
		this.customAction = action;
	}
	
	public void setAction(NetProgramActions action)
	{
		this.action = action;
		this.customAction = null;
	}
	
	public int getlID()
	{
		return lID;
	}
	
	public Runnable getCustomAction()
	{
		return customAction;
	}
	
	public NetProgramActions getAction()
	{
		return action;
	}
}
