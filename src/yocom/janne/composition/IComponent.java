package yocom.janne.composition;

public abstract class IComponent
{
	private NetLayer parent = null;
	
	public void setParent(NetLayer layer)
	{
		parent = layer;
	}
	
	public NetLayer getParent()
	{
		return parent;
	}
}
