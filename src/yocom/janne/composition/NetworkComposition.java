package yocom.janne.composition;

import java.util.ArrayList;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.Synapse;

public class NetworkComposition
{
	protected ArrayList<NetLayer> layers = new ArrayList<NetLayer>();
	private Table<BaseNeuralNetwork, BaseNeuralNetwork, ArrayList<Synapse>> synapses = HashBasedTable.create();
	//private ActionScript script;
	
	public ArrayList<Synapse> getSynapses(BaseNeuralNetwork output, BaseNeuralNetwork input)
	{
		return synapses.get(output, input);
	}
	
	//public ActionScript getScript()
	//{
	//	return script;
	//}
	
	//public void setScript(ActionScript script)
	//{
	///	this.script = script;
	//}
	
	public boolean layerExist(int layerID)
	{
		return layers.size() > layerID && layerID >= 0;
	}
	
	public int layerLength(int layerID)
	{
		return layerExist(layerID) ? getLayer(layerID).length() : -1;
	}

	public NetLayer getLayer(int layerID)
	{
		if(layers.size() > layerID && layerID >= 0)
			return layers.get(layerID);
		else
			return null;
	}
	
	public int numLayers()
	{
		return layers.size();
	}
}
