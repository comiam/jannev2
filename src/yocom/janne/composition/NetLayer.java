package yocom.janne.composition;

import java.util.ArrayList;

import yocom.janne.io.Log;

public class NetLayer extends ArrayList<IComponent>
{
	private static final long serialVersionUID = -3898651466257898071L;
	private NetworkComposition parent;
	private boolean onAdding = false;
	
	public static NetLayer empty()
	{
		NetLayer result = new NetLayer();
		return result;
	}

	@Override
	@Deprecated
	public synchronized boolean add(IComponent net)
	{
		if(onAdding)
		{
			onAdding = false;
			return super.add(net);
		}
		else
		{
			Log.error("This method isn't used in the NetLayer class! Net hasn't been added!");
			return false;
		}
	}
	
	@Override
	@Deprecated
	public synchronized void add(int index, IComponent net)
	{
		if(onAdding)
		{
			onAdding = false;
			net.setParent(this);
			super.add(net);
		}else
			Log.error("This method isn't used in the NetLayer class! Net hasn't been added!");
	}
		
	public boolean addNet(IComponent net)
	{
		onAdding = true;
		return add(net);
	}
	
	public void addNet(int LNID, IComponent net)
	{
		onAdding = true;
		add(LNID, net);
	}
	
	public void removeNet(IComponent bn)
	{
		if(contains(bn))
			remove(bn);
	}
	
	public void removeNet(int NID)
	{
		if(length() > NID && NID >= 0)
			remove(NID);
	}
	
	public int beginNetIndex()
	{
		if(parent == null)
			return -1;
		else
		{
			int begin = 0;
			for(int i = 0;i < parent.numLayers();i++)
			{
				if(!parent.getLayer(i).equals(this))
					begin += parent.getLayer(i).length();
				else break;
			}
				
			return begin;
		}
	}
	
	public int length()
	{
		return this.size();
	}
		
	public void setParent(NetworkComposition parent)
	{
		this.parent = parent;
	}
	
	public int getLayerID()
	{
		return parent == null ? -1 : parent.layers.indexOf(this);
	}
	
	public NetworkComposition getParent()
	{
		return parent;
	}
}

