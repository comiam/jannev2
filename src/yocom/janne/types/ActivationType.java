package yocom.janne.types;

import java.math.BigDecimal;
import java.math.MathContext;

public enum ActivationType
{
	Linear, Threshold, Sigmoid, HyperbolicTangent, SoftPlus, ReLU, PReLU, Sin, Cos, Gaussian, Unknown;
	
	private static int tmpI;
	private static double tmpD;
	private static BigDecimal tmpBD;
	private static MathContext mc = new MathContext(40);

	public static double engine(ActivationType type, double input, double steepness)
	{
		if(Math.abs(steepness) > 1)
			steepness = 1 / steepness;
		switch (type)
		{
			case SoftPlus:
				tmpD = Math.log(1 + Math.exp(input));
				if (tmpD == 0.0 || Double.isInfinite(tmpD))
				{
					try
					{
						BigDecimal dec = new BigDecimal(Math.E, mc);
						tmpI = (int) (input);
						dec.pow(tmpI, mc);
						dec.multiply(new BigDecimal(Math.exp(input - tmpI), mc));
						return Math.log(1 + dec.doubleValue());
					} catch (Throwable e)
					{
						return tmpD;
					}
				} else
					return tmpD;
			case Gaussian:
				tmpD = Math.exp(-input * input * steepness);
				if (tmpD == 0.0 || Double.isInfinite(tmpD))
				{
					try
					{
						BigDecimal dec = new BigDecimal(Math.E, mc);
						tmpI = (int) (-input * input * steepness);
						dec.pow(tmpI, mc);
						dec.multiply(new BigDecimal(Math.exp((-input * input * steepness) - tmpI), mc));
						return dec.doubleValue();
					} catch (Throwable e)
					{
						return tmpD;
					}
				} else
					return tmpD;
			case Sin:
				return Math.sin(input);
			case Cos:
				return Math.cos(input);
			case PReLU:
				if (input < 0)
					return input * steepness;
				else
					return input;
			case ReLU:
				if (input < 0)
					return 0;
				else
					return input;
			case Threshold:
				if (input < steepness)
					return -1;
				else
					return 1;
			case HyperbolicTangent:
				tmpD = (Math.exp(2 * (input / steepness)));
				if (tmpD == 0.0 || Double.isInfinite(tmpD))
				{
					try
					{
						BigDecimal dec = new BigDecimal(Math.E, mc);
						tmpI = (int) (2 * (input / steepness));
						dec.pow(tmpI, mc);
						dec.multiply(new BigDecimal(Math.exp(2 * (input / steepness) - tmpI), mc));
						tmpBD = dec.add(BigDecimal.ZERO);

						return dec.add(BigDecimal.valueOf(-1)).divide(tmpBD.add(BigDecimal.ONE, mc), mc).doubleValue();
					} catch (Throwable e)
					{
						return (tmpD - 1) / (tmpD + 1);
					}
				} else
					return (tmpD - 1) / (tmpD + 1);
			case Linear:
				return input;
			case Sigmoid:
				tmpD = Math.exp(-steepness * input);
				if (tmpD == 0.0 || Double.isInfinite(tmpD))
				{
					try
					{
						tmpBD = new BigDecimal(Math.E, mc);
						tmpI = (int) (-steepness * input);
						tmpBD.pow(tmpI, mc);
						tmpBD.multiply(new BigDecimal(Math.exp((-steepness * input) - tmpI), mc));

						return BigDecimal.valueOf(1).divide(BigDecimal.valueOf(1).add(tmpBD, mc), mc).doubleValue();
					} catch (Throwable e)
					{
						return 1 / (1 + tmpD);
					}
				} else
					return 1 / (1 + tmpD);
			case Unknown:
				return Double.NaN;
		}
		return Double.NaN;
	}
}
