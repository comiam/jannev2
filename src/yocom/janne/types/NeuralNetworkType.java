package yocom.janne.types;

public enum NeuralNetworkType
{
	Perceptron, ReccurentNN, HopfieldNN, Unknown, LSTM
}
