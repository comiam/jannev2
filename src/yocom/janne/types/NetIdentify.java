package yocom.janne.types;

public class NetIdentify
{
	/**
	 * Convert type to int:
	 * 0 - Perceptron
	 * 1 - RNN
	 * 2 - LSTM
	 * 3 - Hopfield NN
	 * -1 - Unknown NN
	 * @param type
	 * @return
	 */
	public static int toIntType(NeuralNetworkType type)
	{
		switch (type)
		{
			case HopfieldNN:
				return 3;
			case LSTM:
				return 2;
			case Perceptron:
				return 0;
			case ReccurentNN:
				return 1;
			case Unknown:
				return -1;
		}
		return -1;
	}
	
	/**
	 * Convert generation to int:
	 * 0 - Default
	 * 1 - Spike
	 * -1 - Unknown
	 * @param type
	 * @return
	 */
	public static int toIntGen(NeuralGen type)
	{
		switch (type)
		{
			case Unknown:
				return -1;
			case Default:
				return 0;
			case Spike:
				return 1;
		}
		return -1;
	}
	
	/**
	 * Convert activation function type to int:
	 * 0 - Sigmoid
	 * 1 - Hyperbolic Tangent
	 * 2 - Sin
	 * 3 - Cos
	 * 4 - SoftPlus
	 * 5 - Threshold
	 * 6 - PReLU
	 * 7 - ReLu
	 * 8 - Linear
	 * 9 - Gaussian
	 * -1 - Unknown
	 * @param type
	 * @return
	 */
	public static int toIntActivation(ActivationType type)
	{
		switch (type)
		{
			case Unknown:
				return -1;
			case Cos:
				return 3;
			case Gaussian:
				return 9;
			case HyperbolicTangent:
				return 1;
			case Linear:
				return 8;
			case PReLU:
				return 6;
			case ReLU:
				return 7;
			case Sigmoid:
				return 0;
			case Sin:
				return 2;
			case SoftPlus:
				return 4;
			case Threshold:
				return 5;
		}
		return -1;
	}
	
	/**
	 * Convert int to type:
	 * 0 - Perceptron
	 * 1 - RNN
	 * 2 - LSTM
	 * 3 - Hopfield NN
	 * -1 - Unknown NN
	 * @param type
	 * @return
	 */
	public static NeuralNetworkType toTypeInt(int type)
	{
		switch (type)
		{
			case 3:
				return NeuralNetworkType.HopfieldNN;
			case 2:
				return NeuralNetworkType.LSTM;
			case 0:
				return NeuralNetworkType.Perceptron;
			case 1:
				return NeuralNetworkType.ReccurentNN;
			case -1:
				return NeuralNetworkType.Unknown;
		}
		return null;
	}
	
	/**
	 * Convert int to generation:
	 * 0 - Default
	 * 1 - Spike
	 * -1 - Unknown
	 * @param type
	 * @return
	 */
	public static NeuralGen toGenInt(int type)
	{
		switch (type)
		{
			case -1:
				return NeuralGen.Unknown;
			case 0:
				return NeuralGen.Default;
			case 1:
				return NeuralGen.Spike;
		}
		return null;
	}
	
	/**
	 * Convert int to activation function type:
	 * 0 - Sigmoid
	 * 1 - Hyperbolic Tangent
	 * 2 - Sin
	 * 3 - Cos
	 * 4 - SoftPlus
	 * 5 - Threshold
	 * 6 - PReLU
	 * 7 - ReLu
	 * 8 - Linear
	 * 9 - Gaussian
	 * -1 - Unknown
	 * @param type
	 * @return
	 */
	public static ActivationType toActivationInt(int type)
	{
		switch (type)
		{
			case -1:
				return ActivationType.Unknown;
			case 3:
				return ActivationType.Cos;
			case 9:
				return ActivationType.Gaussian;
			case 1:
				return ActivationType.HyperbolicTangent;
			case 8:
				return ActivationType.Linear;
			case 6:
				return ActivationType.PReLU;
			case 7:
				return ActivationType.ReLU;
			case 0:
				return ActivationType.Sigmoid;
			case 2:
				return ActivationType.Sin;
			case 4:
				return ActivationType.SoftPlus;
			case 5:
				return ActivationType.Threshold;
		}
		return null;
	}
}
