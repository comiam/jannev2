package yocom.janne.types;

public enum NeuronType
{
	Input, Hidden, Output, IO, Unknown, Reccurent, LSTM;
	
	public static boolean isUsual(NeuronType type)
	{
		return type == NeuronType.Input || type == NeuronType.Hidden || type == Output || type == IO || type == Unknown;
	}
}
