package yocom.janne.types;

/**
 * Over time, the list will be updated.
 */
public enum NetProgramActions
{
	READ_INPUT,
	CALC_INPUT,
	CUSTOM_ACTION,
	HOPFIELD_LAYER_ACTION
}
