package yocom.janne.types;

public enum LogType
{
	INFO,
	DEBUG,
	ERROR
}
