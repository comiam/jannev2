package yocom.janne.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import yocom.janne.io.Log;
import yocom.janne.types.ActivationType;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuronType;

public abstract class BaseNeuron implements Serializable
{
	private static final long serialVersionUID = 4749160479654467314L;
	public static final double DEFAULT_ACTIVATION_STEEPNESS = 1F;
	public static final double DEFAULT_ACTIVATION_BIAS = 0F;
	protected ActivationType activationType = ActivationType.Unknown;
	protected NeuronType neuronType = NeuronType.Unknown;
	protected NeuralGen neuronGen = NeuralGen.Unknown;
	protected double obtainedValue, createdValue;
	protected double delta = 0;
	protected boolean deltaFormed = false;
	
	protected BaseLayer layer;

	protected ArrayList<BaseNeuron> getters = new ArrayList<BaseNeuron>();
	protected ArrayList<BaseNeuron> setters = new ArrayList<BaseNeuron>();
	
	public abstract Map<String, String> getNecessaryData();
	public abstract void implNecessaryData(Map<String, String> map);
	public abstract void initGettersAndSetters(String getters, String setters);
	public abstract void activate();
	public abstract void update();
	public abstract void calculateSettersSum(BaseNeuralNetwork bn);
	
	public NeuronType getNType()
	{
		return neuronType;
	}
	
	public void setNType(NeuronType atype)
	{
		this.neuronType = atype;
	}
	
	public void setGen(NeuralGen gen)
	{
		if(neuronGen == NeuralGen.Unknown)
			this.neuronGen = gen;
	}
	
	public ActivationType getAType()
	{
		return activationType;
	}
	
	public void setAType(ActivationType atype)
	{
		this.activationType = atype;
	}
	
	public ArrayList<BaseNeuron> getters()
	{
		return getters;
	}
	
	public ArrayList<BaseNeuron> setters()
	{
		return setters;
	}
	
	public BaseLayer getParent()
	{
		return layer;
	}
	
	public double delta()
	{
		return delta;
	}
	
	public boolean deltaFormed()
	{
		return deltaFormed;
	}
	
	public double obtained()
	{
		return obtainedValue;
	}
	
	public double created()
	{
		return createdValue;
	}
	
	public int getLayerID()
	{
		return layer == null ? -1 : layer.getLayerID();
	}
	
	public int getLayerNID()
	{
		return layer == null ? -1 : layer.indexOf(this);
	}
	
	public NeuralGen getGen()
	{
		return neuronGen;
	}
	
	public void setDelta(double delta)
	{
		this.delta = delta;
	}
	
	public void setDeltaFormed(boolean deltaFormed)
	{
		this.deltaFormed = deltaFormed;
	}
	
	public void putGetter(BaseNeuron n)
	{
		if(!getters.contains(n) && neuronGen == n.neuronGen)
			getters.add(n);
		else
			Log.error("This neuron generation doesn't match the getter generation!");
	}
	
	public void putSetter(BaseNeuron n)
	{
		if(!setters.contains(n) && neuronGen == n.neuronGen)
			setters.add(n);
		else
			Log.error("This neuron generation doesn't match the setter generation!");
	}
	
	public void removeGetter(BaseNeuron n)
	{
		if(getters.contains(n))
			getters.remove(n);
	}
	
	public void removeSetter(BaseNeuron n)
	{
		if(setters.contains(n))
			setters.remove(n);
	}
	
	public void setParent(BaseLayer parent)
	{
		this.layer = parent;
	}
}
