package yocom.janne.base;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import yocom.janne.calc.Parallel;
import yocom.janne.io.FastScanner;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.neurons.DefNeuron;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuralNetworkType;
import yocom.janne.types.NeuronType;

public abstract class BaseNeuralNetwork implements Serializable
{
	private static final long serialVersionUID = 4679876912274949874L;
	public static final float NEURAL_NETWORK_VERSION = 2.0F;
	public static final float EARLIEST_SUPPORTED_VERSION = 2.0F;
	protected float currentVersion = 2.0F;
	protected NeuralNetworkType networkType = NeuralNetworkType.Unknown;
	protected NeuralGen networkGen = NeuralGen.Unknown;
	protected double currentError = 1;
	protected boolean reseted = true;
	protected boolean inited = false;
	protected ArrayList<BaseLayer> layers = new ArrayList<BaseLayer>();
	protected Table<BaseNeuron, BaseNeuron, Double> weights = HashBasedTable.create();

	public abstract boolean save(String path);

	public abstract void init();

	public BaseNeuron getNeuron(int layer, int layerNID)
	{
		if (layers.size() > layer && layer >= 0 && layers.get(layer).length() > layerNID && layerNID >= 0)
			return layers.get(layer).get(layerNID);
		else
			return null;
	}

	public boolean synapseExist(BaseNeuron output, BaseNeuron input)
	{
		return weights.get(output, input) != null;
	}

	public boolean synapseExist(int outputLID, int outputNID, int inputLID, int inputNID)
	{
		return weights.get(getNeuron(outputLID, outputNID), getNeuron(inputLID, inputNID)) != null;
	}

	public double getWeightSynapse(BaseNeuron output, BaseNeuron input)
	{
		if (weights.get(output, input) != null)
			return weights.get(output, input);
		else
			return Double.NaN;
	}

	public double getWeightSynapse(int outputLID, int outputNID, int inputLID, int inputNID)
	{
		return getWeightSynapse(getNeuron(outputLID, outputNID), getNeuron(inputLID, inputNID));
	}

	public boolean layerExist(int layerID)
	{
		return layers.size() > layerID && layerID >= 0;
	}

	public int layerLength(int layerID)
	{
		return layerExist(layerID) ? getLayer(layerID).length() : -1;
	}

	public BaseLayer getLayer(int layerID)
	{
		if (layers.size() > layerID && layerID >= 0)
			return layers.get(layerID);
		else
			return null;
	}

	public int numLayers()
	{
		return layers.size();
	}

	public int neuronCount()
	{
		int size = 0;
		for (BaseLayer bl : layers)
			size += bl.length();
		return size;
	}

	public int getInputSize()
	{
		if (layers.size() > 0)
			return layers.get(0).length();
		else
			return -1;
	}

	public int getOutputSize()
	{
		if (layers.size() > 1)
			return layers.get(layers.size() - 1).length();
		else
			return -1;
	}

	public float currentVersion()
	{
		return currentVersion;
	}

	public double currentError()
	{
		return currentError;
	}

	public void setCurrentError(double val)
	{
		currentError = val;
	}

	public static BaseNeuralNetwork open(String path)
	{
		if (!path.endsWith(".ann") && !new File(path + ".ann").exists())
		{
			Log.error("Unknown file extension!!!");
			return null;
		}
		File tmp = new File(path);
		tmp = tmp.exists() ? tmp : new File(path + ".ann");
		try (FastScanner scan = new FastScanner(new FileInputStream(tmp)))
		{
			scan.next();
			scan.next();
			scan.next();
			scan.next();
			scan.next();
			scan.next();
			double version = scan.nextDouble();
			scan.next();
			if (version < EARLIEST_SUPPORTED_VERSION || version > NEURAL_NETWORK_VERSION)
			{
				Log.error("This version of neural network not supported!!!");
				return null;
			}
			NeuralGen gen = NeuralGen.valueOf(scan.next());
			scan.next();
			NeuralNetworkType type = NeuralNetworkType.valueOf(scan.next());
			scan.next();
			double currentError = scan.nextDouble();
			scan.next();
			long layerCount = scan.nextLong();
			scan.next();
			long synapseCount = scan.nextLong();
			BaseNeuralNetwork net = null;
			switch (gen)
			{
				case Default:
					switch (type)
					{
						case HopfieldNN:
							break;
						case LSTM:
							break;
						case Perceptron:
							net = new DefNeuralNetwork();
							net.init();
							net.networkType = type;
							net.currentError = currentError;
							break;
						case ReccurentNN:
							break;
						case Unknown:
							break;
					}
					break;
				case Spike:
					break;
				case Unknown:
					break;
			}
			if (net == null)
			{
				Log.error("Type " + type.name() + " of generation " + gen.name() + " not implemented in this version!");
				return null;
			}
			Map<String, String> neuronData = null;
			switch (gen)
			{
				case Default:
					switch (type)
					{
						case HopfieldNN:
							break;
						case LSTM:
							break;
						case Perceptron:
							neuronData = new DefNeuron().getNecessaryData();
							break;
						case ReccurentNN:
							break;
						case Unknown:
							break;
					}
					break;
				case Spike:
					break;
				case Unknown:
					break;
			}
			scan.next();
			scan.next();
			scan.next();
			BaseLayer bl;

			int neuronCount, neuronDataLength;
			NeuronType lt;
			BaseNeuron bn = null;
			String var, val, strArr = "";
			Map<String, String> map2 = new HashMap<String, String>();
			Map<BaseNeuron, String[]> gsSet = new HashMap<BaseNeuron, String[]>();
			for (int i = 0; i < layerCount; i++)
			{
				scan.next();
				scan.next();
				neuronCount = scan.nextInt();
				lt = NeuronType.valueOf(scan.next());
				scan.next();
				bl = BaseLayer.empty(lt, gen);
				bl.setParent(net);
				for (int j = 0; j < neuronCount; j++)
				{
					scan.next();
					scan.next();
					neuronDataLength = scan.nextInt();
					map2.clear();
					map2.putAll(neuronData);
					for (int j2 = 0; j2 < neuronDataLength; j2++)
					{
						var = scan.next();
						if (map2.containsKey(var.trim().replaceAll(":", "")))
						{
							if (var.trim().replaceAll(":", "").equals("getters")
									|| var.trim().replaceAll(":", "").equals("setters"))
							{
								while (!(val = scan.next()).equals("|"))
									strArr += val + " ";
								map2.put(var.trim().replaceAll(":", ""), strArr);
								strArr = "";
							} else
								map2.put(var.trim().replaceAll(":", ""), scan.next().trim());
						} else if (var.equals("endData."))
							throw new NullPointerException("Damaged neuron data " + j + " of layer " + i);
					}
					scan.next();
					switch (gen)
					{
						case Default:
							switch (type)
							{
								case HopfieldNN:
									break;
								case LSTM:
									break;
								case Perceptron:
									bn = new DefNeuron(map2);
									break;
								case ReccurentNN:
									break;
								case Unknown:
									break;
							}
							break;
						case Spike:
							break;
						case Unknown:
							break;
					}
					bl.addNeuron(bn);
					gsSet.put(bn, new String[] { map2.get("getters"), map2.get("setters") });
				}
				net.layers.add(bl);
			}
			Parallel.forEach(gsSet.entrySet(), true, (cell) -> {
				cell.getKey().initGettersAndSetters(cell.getValue()[0], cell.getValue()[1]);
			});

			gsSet.clear();
			gsSet = null;

			scan.next();
			for (int i = 0; i < synapseCount; i++)
			{
				net.weights.put(net.getNeuron(scan.nextInt(), scan.nextInt()),
						net.getNeuron(scan.nextInt(), scan.nextInt()), scan.nextDouble());
			}
			switch (gen)// load another data for special networks
			{
				case Default:
					switch (type)
					{
						case HopfieldNN:
							break;
						case LSTM:
							break;
						case Perceptron:
							break;
						case ReccurentNN:
							break;
						case Unknown:
							break;
					}
					break;
				case Spike:
					break;
				case Unknown:
					break;
			}
			Log.info(net.networkType + " loaded successfully.");
			System.gc();
			return net;
		} catch (Exception e)
		{
			Log.error("Error during opening file!!!", e);
			System.gc();
			return null;
		}
	}

	public static BaseNeuralNetwork open(File file)
	{
		return open(file.getPath());
	}

	public NeuralGen getGen()
	{
		return networkGen;
	}

	public NeuralNetworkType getType()
	{
		return networkType;
	}

	public boolean reseted()
	{
		return reseted;
	}

	public void refreshLayers()
	{
		for (int i = 0; i < numLayers(); i++)
		{
			if (NeuronType.isUsual(getLayer(i).getType()) && i == 0 && getLayer(i).getType() != NeuronType.Input
					&& getLayer(i).getType() != NeuronType.IO)
			{
				getLayer(i).setType(NeuronType.Input);
			} else if (NeuronType.isUsual(getLayer(i).getType()) && i == numLayers() - 1
					&& getLayer(i).getType() != NeuronType.Output && getLayer(i).getType() != NeuronType.IO)
			{
				getLayer(i).setType(NeuronType.Output);
			} else if (NeuronType.isUsual(getLayer(i).getType()) && i != numLayers() - 1 && i != 0)
			{
				getLayer(i).setType(NeuronType.Hidden);
			}
		}
	}

	public void resetObtainedValues()
	{
		if (reseted)
			return;
		reseted = true;
		Parallel.forEach(layers, true, (layer) -> {
			Parallel.forEach(layer, true, (neuron) -> {
				neuron.obtainedValue = 0;
			});
		});
	}

	@Override
	public BaseNeuralNetwork clone()
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream ous = new ObjectOutputStream(baos);
			ous.writeObject(this);
			ous.flush();
			ous.close();
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			Log.info("The cloning of " + networkType.name() + " is finished!");
			return (BaseNeuralNetwork) ois.readObject();
		} catch (Throwable ex)
		{
			Log.error("Copy of neural network failed!", ex);
			return null;
		}
	}
}
