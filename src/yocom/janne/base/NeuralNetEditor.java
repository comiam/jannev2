package yocom.janne.base;

import yocom.janne.io.Log;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuralNetworkType;
import yocom.janne.types.NeuronType;
import yocom.janne.util.MathUtils;

public class NeuralNetEditor
{
	public static boolean createSynapse(BaseNeuralNetwork net, int outputLID, int outputNID, int inputLID, int inputNID,
			boolean onCreating)
	{
		return createSynapse(net, net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID), MathUtils.randomDouble(-0.5, 0.5),
				onCreating);
	}

	public static boolean createSynapse(BaseNeuralNetwork net, int outputLID, int outputNID, int inputLID, int inputNID,
			double weight, boolean onCreating)
	{
		return createSynapse(net, net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID), weight,
				onCreating);
	}

	public static boolean createSynapse(BaseNeuralNetwork net, BaseNeuron output, BaseNeuron input, double weight,
			boolean onCreating)
	{
		if (net.weights.get(output, input) != null)
		{
			Log.error("This synapse doesn't exist!");
			return false;
		}
		output.putGetter(input);
		input.putSetter(output);
		if (net.networkType != NeuralNetworkType.HopfieldNN)
		{
			output.update();
			input.update();
		}
		net.weights.put(output, input, weight);
		
		if (!onCreating)
			NeuralNetTools.analizeAndFix(net);
		return true;
	}

	public static boolean addSynapse(BaseNeuralNetwork net, Synapse synapse, boolean onCreating)
	{
		return createSynapse(net, synapse.getSetter(), synapse.getGetter(), synapse.getWeight(), onCreating);
	}

	public static boolean createSynapse(BaseNeuralNetwork net, BaseNeuron output, BaseNeuron input, boolean onCreating)
	{
		return createSynapse(net, output, input, MathUtils.randomDouble(-0.5, 0.5), onCreating);
	}

	public static void removeSynapses(BaseNeuralNetwork net, Synapse[] weights)
	{
		for (Synapse s : weights)
			removeSynapse(net, s.getSetter(), s.getGetter());
	}

	public static double removeSynapse(BaseNeuralNetwork net, int outputLID, int outputNID, int inputLID, int inputNID)
	{
		if (net.weights.get(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID)) == null)
		{
			Log.error("This synapse doesn't exist!");
			return Double.NaN;
		}
		net.getNeuron(outputLID, outputNID).removeGetter(net.getNeuron(inputLID, inputNID));
		net.getNeuron(inputLID, inputNID).removeSetter(net.getNeuron(outputLID, outputNID));
		net.getNeuron(outputLID, outputNID).update();
		net.getNeuron(inputLID, inputNID).update();

		double wPrev = net.weights.remove(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID));
		NeuralNetTools.analizeAndFix(net);
		return wPrev;
	}

	public static double removeSynapse(BaseNeuralNetwork net, BaseNeuron output, BaseNeuron input)
	{
		if (net.weights.get(output, input) == null)
		{
			Log.error("This synapse doesn't exist!");
			return Double.NaN;
		}
		output.getters.remove(input);
		input.setters.remove(output);
		output.update();
		input.update();

		double wPrev = net.weights.remove(output, input);

		NeuralNetTools.analizeAndFix(net);

		return wPrev;
	}

	public static boolean setWeightSynapse(BaseNeuralNetwork net, int outputLID, int outputNID, int inputLID,
			int inputNID, double newWeight)
	{
		if (net.weights.get(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID)) == null)
		{
			Log.error("This synapse doesn't exist!");
			return false;
		}
		net.weights.put(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID), newWeight);
		return true;
	}

	public static boolean changeWeightSynapse(BaseNeuralNetwork net, int outputLID, int outputNID, int inputLID,
			int inputNID, double deltaWeight)
	{
		if (net.weights.get(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID)) == null)
		{
			Log.error("This synapse doesn't exist!");
			return false;
		}

		double wPrev = net.weights.remove(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID));
		net.weights.put(net.getNeuron(outputLID, outputNID), net.getNeuron(inputLID, inputNID), wPrev + deltaWeight);
		return true;
	}

	public static boolean changeWeightSynapse(BaseNeuralNetwork net, BaseNeuron output, BaseNeuron input,
			double deltaWeight)
	{
		return changeWeightSynapse(net, output.getLayerID(), output.getLayerNID(), input.getLayerID(),
				input.getLayerNID(), deltaWeight);
	}

	public static boolean putLayer(BaseNeuralNetwork net, BaseLayer bl)
	{
		if (bl.ngen != net.networkGen)
		{
			Log.error("This layer generation doesn't match the network generation!");
			return false;
		}
		if (bl.getLayerID() == -1)
		{
			net.layers.add(bl);
			bl.setParent(net);
		} else
		{
			net.layers.add(bl.getLayerID(), bl);
			bl.setParent(net);
		}
		return true;
	}

	public static boolean putLayer(BaseNeuralNetwork net, int layerID, BaseLayer bl)
	{
		if (bl.ngen != net.networkGen)
		{
			Log.error("This layer generation doesn't match the network generation!");
			return false;
		}
		if (net.layers.size() != 0 && net.layers.size() > layerID && layerID >= 0)
		{
			net.layers.add(bl.getLayerID(), bl);
			bl.setParent(net);
		} else
		{
			net.layers.add(bl);
			bl.setParent(net);
		}
		return true;
	}

	public static void setGen(BaseNeuralNetwork net, NeuralGen gen)
	{
		net.networkGen = gen;
	}

	public static void setType(BaseNeuralNetwork net, NeuralNetworkType type)
	{
		net.networkType = type;
	}

	public static void setReseted(BaseNeuralNetwork net, boolean reseted)
	{
		net.reseted = reseted;
	}

	public static void updateCurrentError(BaseNeuralNetwork net, double newError)
	{
		net.currentError = newError;
	}

	public static boolean addNeuron(BaseNeuralNetwork net, BaseNeuron dn, boolean haveParent)
	{
		if (dn.neuronGen == net.networkGen)
		{
			Log.error("This neuron is not suitable for this neural network!");
			return false;
		}
		if (haveParent)
		{
			if (dn.neuronGen == net.networkGen && dn.getParent() != null && net.layers.contains(dn.getParent())
					&& dn.getParent().getLayerID() != -1)
			{
				return dn.getParent().addNeuron(dn);
			} else
			{
				Log.error("A neuron must be addressable in the network!");
				return false;
			}
		} else
		{
			switch (dn.neuronType)
			{
				case Hidden:
					for (BaseLayer bl : net.layers)
						if (bl.getType() == dn.neuronType)
						{
							bl.addNeuron(dn);
							break;
						}
					if (putLayer(net, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
						net.layers.get(net.layers.size() - 1).addNeuron(dn);
					break;
				case IO:
					if (net.layers.get(0).mainType != NeuronType.IO
							&& net.layers.get(net.layers.size() - 1).mainType != NeuronType.IO)
					{
						if (putLayer(net, 0, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
							net.layers.get(0).addNeuron(dn);
					} else if (net.layers.get(0).mainType == NeuronType.IO)
					{
						net.layers.get(0).addNeuron(dn);
					} else if (net.layers.get(net.layers.size() - 1).mainType == NeuronType.IO)
					{
						net.layers.get(net.layers.size() - 1).addNeuron(dn);
					}
					break;
				case Input:
					if (net.layers.get(0).mainType != NeuronType.Input)
					{
						if (putLayer(net, 0, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
							net.layers.get(0).addNeuron(dn);
					} else
						net.layers.get(0).addNeuron(dn);
					break;
				case LSTM:
					for (BaseLayer bl : net.layers)
						if (bl.getType() == dn.neuronType)
						{
							bl.addNeuron(dn);
							break;
						}
					if (putLayer(net, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
						net.layers.get(net.layers.size() - 1).addNeuron(dn);
					break;
				case Output:
					if (net.layers.get(net.layers.size() - 1).mainType != NeuronType.Output)
					{
						if (putLayer(net, net.layers.size() - 1, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
							net.layers.get(net.layers.size() - 1).addNeuron(dn);
					} else
						net.layers.get(net.layers.size() - 1).addNeuron(dn);
					break;
				case Reccurent:
					for (BaseLayer bl : net.layers)
						if (bl.getType() == dn.neuronType)
						{
							bl.addNeuron(dn);
							break;
						}
					if (putLayer(net, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
						net.layers.get(net.layers.size() - 1).addNeuron(dn);
					break;
				case Unknown:
					for (BaseLayer bl : net.layers)
						if (bl.getType() == dn.neuronType)
						{
							bl.addNeuron(dn);
							break;
						}
					if (putLayer(net, BaseLayer.empty(dn.neuronType, dn.neuronGen)))
						net.layers.get(net.layers.size() - 1).addNeuron(dn);
					break;
			}
		}

		return false;
	}

	public static void addNeuron(BaseNeuralNetwork net, BaseNeuron dn, int layerID, int layerNID)
	{
		if (dn.neuronGen == net.networkGen && net.layerExist(layerID) && net.layerLength(layerID) > layerNID)
		{
			BaseLayer tmp = net.getLayer(layerID);
			tmp.addNeuron(layerNID, dn);
			if (!tmp.equals(dn.getParent()))
				dn.setParent(tmp);
		} else
		{
			if (dn.neuronGen != net.networkGen)
				Log.error("This neuron is not suitable for this neural network!");
			if (!net.layerExist(layerID))
				Log.error("Layer " + layerID + " doesn't exist!");
			if (net.layerLength(layerID) <= layerNID)
				Log.error("The length of the layer <= index of the neuron!");
		}
	}
}
