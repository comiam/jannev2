package yocom.janne.base;

import java.util.ArrayList;

import yocom.janne.action.NetAction;
import yocom.janne.io.Log;
import yocom.janne.types.NetProgramActions;

public class NeuralNetProgram
{
	private ArrayList<NetAction> actionList;
	private int numLayers = 0;
	private long iterations = 0;
	private boolean containsCustomActions = false;
	private boolean buildNow = false;

	/**
	 * Starts building a new net program.
	 * 
	 * @param net
	 *            - the network for which the program is built
	 */
	public void buildNew(BaseNeuralNetwork net)
	{
		this.numLayers = net.numLayers();
		if (numLayers == 0)
		{
			Log.error("Net is empty!");
			return;
		}
		if (actionList != null && !actionList.isEmpty())
		{
			actionList.clear();
			actionList = null;

		}

		this.actionList = new ArrayList<NetAction>();
		this.containsCustomActions = false;
		this.buildNow = true;
	}

	/**
	 * Ends building a new program.
	 */
	public void endBuild()
	{
		buildNow = false;
	}
	
	/**
	 * Used for HopfiledNN
	 * @param value - iterations
	 */
	public void setIterations(long value)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		this.iterations = value;
	}

	/**
	 * The method determines the sequence to the performer, where each layer will
	 * execute the command that you pass in the arguments.
	 * 
	 * @param actions
	 *            - custom actions that you pass. Actions must be set in a strict
	 *            sequence in arguments
	 */
	public void forEachAllLayers(Runnable... actions)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		forCustom();
		for (int i = 0; i < numLayers; i++)
			for (Runnable a : actions)
			{
				actionList.add(new NetAction(a, i));
			}
	}

	/**
	 * The method determines the sequence to the performer, where each layer will
	 * execute the command that you pass in the arguments.
	 * 
	 * @param actions
	 *            - base actions that you pass. Actions must be set in a strict
	 *            sequence in arguments
	 */
	public void forEachAllLayers(NetProgramActions... actions)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		for (int i = 0; i < numLayers; i++)
			for (NetProgramActions a : actions)
			{
				actionList.add(new NetAction(a, i));
			}
	}

	/**
	 * The method defines a sequence to the executor, where the layers of the range
	 * you select will execute the command you pass in the arguments.
	 * 
	 * @param begin
	 *            - the begin of range
	 * @param end
	 *            - the end of the range. The end is also included in the range.
	 * @param performers
	 *            - custom actions that you pass. Actions must be set in a strict
	 *            sequence in arguments
	 */
	public void forEachLayers(int begin, int end, Runnable... actions)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		if (begin < 0)
		{
			Log.error("begin < 0");
			return;
		}
		if (end >= numLayers)
		{
			Log.error("end > last layer ID!");
			return;
		}
		if (!actionList.isEmpty() && actionList.get(actionList.size() - 1).getlID() < begin - 1)
		{
			Log.error("Add operations for the previous layers first!");
			return;
		}
		forCustom();
		for (int i = begin; i <= end; i++)
			for (Runnable a : actions)
			{
				actionList.add(new NetAction(a, i));
			}
	}

	/**
	 * The method defines a sequence to the executor, where the layers of the range
	 * you select will execute the command you pass in the arguments.
	 * 
	 * @param begin
	 *            - the begin of range
	 * @param end
	 *            - the end of the range. The end is also included in the range.
	 * @param performers
	 *            - base actions that you pass. Actions must be set in a strict
	 *            sequence in arguments
	 */
	public void forEachAllLayers(int begin, int end, NetProgramActions... actions)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		if (begin < 0)
		{
			Log.error("begin < 0");
			return;
		}
		if (end >= numLayers)
		{
			Log.error("end > last layer ID!");
			return;
		}
		if (!actionList.isEmpty() && actionList.get(actionList.size() - 1).getlID() < begin - 1)
		{
			Log.error("Add operations for the previous layers first!");
			return;
		}

		for (int i = begin; i <= end; i++)
			for (NetProgramActions a : actions)
			{
				actionList.add(new NetAction(a, i));
			}
	}

	/**
	 * Sets the list of custom commands to the layer.
	 * 
	 * @param lID
	 *            - layer ID
	 * @param actions
	 *            - list of custom commands to the layer
	 */
	public void setActionsForLayer(int lID, Runnable... actions)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		if (lID < 0)
		{
			Log.error("layer ID < 0");
			return;
		}
		if (lID >= numLayers)
		{
			Log.error("layer ID > last layer ID!");
			return;
		}
		if ((!actionList.isEmpty() && actionList.get(actionList.size() - 1).getlID() < lID - 1)
				|| (actionList.isEmpty() && lID != 0))
		{
			Log.error("Add operations for the previous layers first!");
			return;
		}

		forCustom();
		for (Runnable a : actions)
		{
			actionList.add(new NetAction(a, lID));
		}
	}

	/**
	 * Sets the list of base commands to the layer.
	 * 
	 * @param lID
	 *            - layer ID
	 * @param actions
	 *            - list of base commands to the layer
	 */
	public void setActionsForLayer(int lID, NetProgramActions... actions)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		if (lID < 0)
		{
			Log.error("layer ID < 0");
			return;
		}
		if (lID >= numLayers)
		{
			Log.error("layer ID > last layer ID!");
			return;
		}
		if ((!actionList.isEmpty() && actionList.get(actionList.size() - 1).getlID() < lID - 1)
				|| (actionList.isEmpty() && lID != 0))
		{
			Log.error("Add operations for the previous layers first!");
			return;
		}

		forCustom();
		for (NetProgramActions a : actions)
		{
			actionList.add(new NetAction(a, lID));
		}
	}

	/**
	 * Removes all actions for the specified layer.
	 * 
	 * @param lID
	 *            - layer ID
	 */
	public void removeActionOfLayer(int lID)
	{
		if (!buildNow)
		{
			Log.error("The program is not being built now!");
			return;
		}
		if (lID < 0)
		{
			Log.error("layer ID < 0");
			return;
		}
		if (lID >= numLayers)
		{
			Log.error("layer ID > last layer ID!");
			return;
		}
		if (lID >= actionList.size())
		{
			Log.error("There are no commands for this layer.");
			return;
		}
		final ArrayList<NetAction> trashbox = new ArrayList<NetAction>();
		actionList.parallelStream().forEachOrdered((action) -> {
			if (action.getlID() == lID)
				trashbox.add(action);
		});
		trashbox.parallelStream().forEachOrdered((action) -> {
			actionList.remove(action);
		});
		trashbox.clear();
		checkForCustoms();
	}

	public ArrayList<NetAction> getActions()
	{
		if (buildNow)
		{
			Log.error("The program is building!");
			return null;
		}
		return actionList;
	}

	private void checkForCustoms()
	{
		boolean flag = false;
		for (NetAction na : actionList)
		{
			if (na.getAction() == NetProgramActions.CUSTOM_ACTION)
				flag = true;
			if (flag)
				break;
		}
		if (flag)
			forCustom();
		else
			notForCustom();
	}

	private void forCustom()
	{
		containsCustomActions = true;
	}

	private void notForCustom()
	{
		containsCustomActions = false;
	}

	/**
	 * Checks the program to see if the program has any custom actions for layers.
	 * This determines whether it is possible to run the network on the graphics
	 * card or in native code. If there are such actions, it will be impossible.
	 * 
	 * @return does the program contain custom actions or not.
	 */
	public boolean containsCustomActions()
	{
		return containsCustomActions;
	}
	
	/**
	 * Used for HopfiledNN
	 */
	public long getIterations()
	{
		return iterations;
	}
}
