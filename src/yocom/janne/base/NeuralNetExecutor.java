package yocom.janne.base;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import yocom.janne.action.NetAction;
import yocom.janne.calc.GPUExecutor;
import yocom.janne.calc.NativeExecutor;
import yocom.janne.calc.Parallel;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.types.NeuronType;

public class NeuralNetExecutor
{
	private static boolean logging = true;
	private static boolean useGPU = true;
	private static boolean useNative = true;

	public static void setLogging(boolean training)
	{
		logging = training;
	}

	public static void allowGPUComputing(boolean flag)
	{
		useGPU = flag;
	}

	public static void allowNativeComputing(boolean flag)
	{
		useNative = flag;
	}
	
	public static double[] run(BaseNeuralNetwork net, NeuralNetProgram prog, ArrayList<Double> inputData)
	{
		double[] t = new double[inputData.size()];
		for (int i = 0; i < inputData.size(); i++)
			t[i] = inputData.get(i);
		return run(net, prog, t);
	}

	public static double[] run(BaseNeuralNetwork net, NeuralNetProgram prog, double... inputData)
	{
		if (net == null)
		{
			Log.error("Null neural net!!!");
			return null;
		}

		if (prog == null)
		{
			Log.error("Null net program!!!");
			return null;
		}

		if (inputData == null)
		{
			Log.error("Null input data!!!");
			return null;
		}

		if (logging)
			Log.info("Getting ready to start the neural network.");

		switch (net.networkGen)
		{
			case Default:
				if (!(net instanceof DefNeuralNetwork))
				{
					Log.error("This network isn't a child class of the DefNeuralNetwork!!!");
					break;
				}
				switch (net.networkType)
				{
					case Unknown:
						Log.error("Unknown type of neural net!");
						break;
					default:
						return exec(net, prog, inputData);
				}
				break;
			case Spike:
				// FIXME
				break;
			case Unknown:
				Log.error("Unknown generation of neural net!");
				break;
		}
		return null;
	}

	private static double[] exec(BaseNeuralNetwork net, NeuralNetProgram prog, double... inputData)
	{
		if (!netCheck(net, inputData))
			return null;

		net.resetObtainedValues();

		if (!prog.containsCustomActions() && useGPU && GPUExecutor.GPUAvailable())
			return GPUExecutor.execute(net, prog, inputData);
		else if (!prog.containsCustomActions() && useNative && NativeExecutor.nativeAvailable())
			return NativeExecutor.execute(net, prog, inputData);
		else
			return execInJava(net, prog, inputData);
	}

	private static boolean netCheck(BaseNeuralNetwork net, double... inputData)
	{
		if (net.numLayers() == 0)
		{
			Log.error("No layers in the neural network!");
			return false;
		}

		if (net.neuronCount() == 0)
		{
			Log.error("No neurons in the neural network!");
			return false;
		}

		if (net.getLayer(0).getType() != NeuronType.Input && net.getLayer(0).getType() != NeuronType.IO)
		{
			Log.error("There is no input layer in the neural network!");
			return false;
		}

		if (net.getLayer(net.numLayers() - 1).getType() != NeuronType.Output && net.getLayer(net.numLayers() - 1).getType() != NeuronType.IO)
		{
			Log.error("There is no output layer in the neural network!");
			return false;
		}

		if (net.getLayer(0).length() != inputData.length)
		{
			Log.error("The number of inputs doesn't match the number of neurons in the input layer!");
			return false;
		}

		return true;
	}

	private static double[] execInJava(BaseNeuralNetwork net, NeuralNetProgram prog, double... inputData)
	{
		final List<BaseNeuron> inputs = new ArrayList<BaseNeuron>();
		final List<BaseNeuron> outputs = new ArrayList<BaseNeuron>();

		if (net.numLayers() == 1 && net.getLayer(0).getType() == NeuronType.IO)
		{
			inputs.addAll(net.getLayer(0));
			outputs.addAll(inputs);
		} else
		{
			inputs.addAll(net.getLayer(0));
			outputs.addAll(net.getLayer(net.numLayers() - 1));
		}

		Parallel.makeFor(0, inputs.size(), (i) -> {
			inputs.get(i).obtainedValue = inputData[i];
		});
		
		if (logging)
			Log.info("The end of the preparation. Launch of the " + net.getType().name() + ".");

		for (NetAction a : prog.getActions())
		{
			switch (a.getAction())
			{
				case CALC_INPUT:
					net.getLayer(a.getlID()).calcInput();
					break;
				case CUSTOM_ACTION:
					a.getCustomAction().run();
					break;
				case HOPFIELD_LAYER_ACTION:
					final double[] oldCondition = inputData.clone();
					boolean endSuccessufully = false;
					boolean[] equals = new boolean[1];
					BaseLayer l = net.getLayer(a.getlID());
					
					for(int count = 0;count < prog.getIterations();count++)
					{
						l.readInput();
						l.calcInput();
						
						equals[0] = true;
						Parallel.makeFor(0, l.length(), (i) -> {
							if(equals[0] && l.get(i).createdValue != oldCondition[i])
								equals[0] = false;
						});
						if(equals[0])
						{
							endSuccessufully = true;
							break;
						}else
						{
							Parallel.makeFor(0, l.length(), (i) -> {
								oldCondition[i] = l.get(i).createdValue;
							});
						}
					}
					if ((logging && !endSuccessufully) || Arrays.equals(oldCondition, inputData))
						Log.error("The network was unable to recognize the image!");
					break;
				case READ_INPUT:
					net.getLayer(a.getlID()).readInput();
					break;
			}
		}

		if (logging)
			Log.info("End of work of the " + net.getType().name() + ". Preparing the output.");

		double[] result = new double[outputs.size()];
		Parallel.makeFor(0, result.length, (i) -> {
			result[i] = outputs.get(i).createdValue;
		});

		net.reseted = false;
		if (logging)
			Log.info("The output preparation is complete.");

		return result;
	}
}
