package yocom.janne.base;

import java.io.Serializable;

public class Synapse implements Serializable
{
	private static final long serialVersionUID = -5628686429618686547L;
	private BaseNeuron neuron0, neuron1;
	private double weight;

	public Synapse(BaseNeuron setter, BaseNeuron getter)
	{
		this.neuron0 = setter;
		this.neuron1 = getter;
	}

	public Synapse(BaseNeuron setter, BaseNeuron getter, double weight)
	{
		this.neuron0 = setter;
		this.neuron1 = getter;
		this.weight = weight;
	}
	
	public void setWeight(double weight)
	{
		this.weight = weight;
	}
	
	public void setGetter(BaseNeuron getter)
	{
		this.neuron1 = getter;
	}
	
	public void setSetter(BaseNeuron setter)
	{
		this.neuron0 = setter;
	}
	
	public double getWeight()
	{
		return weight;
	}
	
	public BaseNeuron getGetter()
	{
		return neuron1;
	}
	
	public BaseNeuron getSetter()
	{
		return neuron0;
	}
}
