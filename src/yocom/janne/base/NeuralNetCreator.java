package yocom.janne.base;

import java.util.ArrayList;

import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.types.ActivationType;
import yocom.janne.types.NetProgramActions;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuralNetworkType;
import yocom.janne.types.NeuronType;

public class NeuralNetCreator
{
	private static boolean logging = true;

	public static void setLogging(boolean training)
	{
		logging = training;
	}

	public static DefNeuralNetwork createDefPerceptron(final ArrayList<BaseLayer> layers, boolean organizeBias)
	{
		if (layers.size() == 0)
		{
			Log.error("No layers in arg!");
			return null;
		}

		for (BaseLayer layer : layers)
		{
			if (layer == null)
			{
				Log.error("Null layer " + layers.indexOf(layer) + "!");
				return null;
			}
			if (layer.isEmpty())
			{
				Log.error("The neural network mustn't contain empty layers!");
				return null;
			}
		}

		if (logging)
			Log.info("The creation Perseptron began.");

		NeuralNetBuilder nb = new NeuralNetBuilder(NeuralGen.Default, NeuralNetworkType.Perceptron);
		nb.fullConnectivityToNext();
		for (BaseLayer layer : layers)
			nb.addLayer(layer);

		DefNeuralNetwork net = (DefNeuralNetwork) nb.getNet();
		nb.destroy();

		NeuralNetTools.organizeWeights(net);
		if (organizeBias)
			NeuralNetTools.organizeBias(net);

		System.gc();
		if (logging)
			Log.info("The perceptron created.");

		return net;
	}

	public static NeuralNetProgram getProgramForPerceptron(BaseNeuralNetwork net)
	{
		NeuralNetProgram prog = new NeuralNetProgram();
		prog.buildNew(net);
		prog.forEachAllLayers(NetProgramActions.READ_INPUT, NetProgramActions.CALC_INPUT);
		prog.endBuild();
		return prog;
	}

	public static NeuralNetProgram getProgramForHopfieldNN(BaseNeuralNetwork net)
	{
		NeuralNetProgram prog = new NeuralNetProgram();
		prog.buildNew(net);
		prog.setActionsForLayer(0, NetProgramActions.HOPFIELD_LAYER_ACTION);
		prog.setIterations(10000);
		prog.endBuild();
		return prog;
	}

	public static DefNeuralNetwork createHopfieldNN(int neuronCount)
	{
		if (neuronCount <= 1)
		{
			Log.error("The Hopfield neural network must have more than 1 neuron!!!");
			return null;
		}
		if (logging)
			Log.info("The creation HopfieldNN began.");

		NeuralNetBuilder nb = new NeuralNetBuilder(NeuralGen.Default, NeuralNetworkType.HopfieldNN);
		nb.addLayer(BaseLayer.create(neuronCount, ActivationType.Threshold, NeuronType.IO, NeuralGen.Default, 0, 0));
		nb.connectNeuronsOfLastLayer(false);
		DefNeuralNetwork net = (DefNeuralNetwork) nb.getNet();
		nb.destroy();

		if (logging)
			Log.info("The HopfieldNN created.");

		return net;
	}
}
