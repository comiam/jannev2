package yocom.janne.base;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Table;

import yocom.janne.calc.Parallel;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.neurons.DefNeuron;
import yocom.janne.types.NetIdentify;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuronType;
import yocom.janne.util.MathUtils;

public class NeuralNetTools {
	public static NetSorter comparator = new NetSorter();
	private static HashBasedTable<Integer, Integer, Boolean> checked = HashBasedTable.create();

	public static void sort(List<BaseNeuron> n) {
		Collections.sort(n, comparator);
	}

	public static void sort(BaseNeuron[] n) {
		Arrays.sort(n, comparator);
	}

	public static void resetWeights(BaseNeuralNetwork net) {
		Parallel.makeFor(0, net.weights.size(), (i) -> {
			BaseNeuron n0 = Iterables.get(net.weights.cellSet(), i).getRowKey();
			BaseNeuron n1 = Iterables.get(net.weights.cellSet(), i).getColumnKey();

			net.weights.remove(n0, n1);
			if (n0.getLayerNID() != n1.getLayerNID()
					|| (n0.getLayerNID() == n1.getLayerNID() && n0.getLayerID() != n1.getLayerID()))
				net.weights.put(n0, n1, MathUtils.randomDouble(-0.5, 0.5));
			else
				net.weights.put(n0, n1, MathUtils.randomDouble(-0.1, 0.1));
		});
	}

	public static void resetBias(DefNeuralNetwork net) {
		for (int i = 0; i < net.numLayers(); i++) {
			Parallel.forEach(net.getLayer(i), false, (n) -> {
				((DefNeuron) n).setBias(BaseNeuron.DEFAULT_ACTIVATION_BIAS);
			});
		}
	}

	public static void organizeBias(DefNeuralNetwork net) {
		if (net.numLayers() <= 1) {
			Log.error("The neural network isn't ready for the organization of weights!");
			return;
		}
		int inputs = net.getInputSize();
		int hidden = net.neuronCount() - net.getInputSize() - net.getOutputSize();
		double scaleFactor = 0.7 * Math.pow(hidden, 1D / inputs);

		for (int i = 0; i < net.numLayers(); i++) {
			if (net.getLayer(i).getType() == NeuronType.Hidden)
				Parallel.forEach(net.getLayer(i), false, (n) -> {
					((DefNeuron) n).setBias(MathUtils.randomDouble(-scaleFactor, scaleFactor));
				});
		}
	}

	public static void organizeWeights(DefNeuralNetwork net) {
		if (net.numLayers() <= 1) {
			Log.error("The neural network isn't ready for the organization of weights!");
			return;
		}
		int inputs = net.getInputSize();
		int hidden = net.neuronCount() - net.getInputSize() - net.getOutputSize();
		double scaleFactor = 0.7 * Math.pow(hidden, 1D / inputs);
		final double[] rootWeightSum = new double[1];
		final Table<BaseNeuron, BaseNeuron, Double> deltas = HashBasedTable.create();

		for (int i = 0; i < net.numLayers(); i++) {
			if (net.getLayer(i).getType() == NeuronType.Hidden)
				Parallel.forEach(net.getLayer(i), false, (n) -> {
					Parallel.forEach(n.setters, false, (setterl) -> {
						rootWeightSum[0] += net.getWeightSynapse(setterl, n) * net.getWeightSynapse(setterl, n);
					});
					rootWeightSum[0] = Math.pow(rootWeightSum[0], 0.5);
					Parallel.forEach(n.setters, false, (setterl) -> {
						deltas.put(setterl, n, (scaleFactor * net.getWeightSynapse(setterl, n)) / rootWeightSum[0]
								- net.getWeightSynapse(setterl, n));
					});
					rootWeightSum[0] = 0;
				});
		}

		for (int i = 0; i < net.numLayers(); i++) {
			if (net.getLayer(i).getType() == NeuronType.Hidden)
				Parallel.forEach(net.getLayer(i), false, (n) -> {
					Parallel.forEach(n.setters, false, (setter) -> {
						NeuralNetEditor.changeWeightSynapse(net, setter, n, deltas.get(setter, n));
					});
				});
		}
	}

	public static void analizeAndFix(BaseNeuralNetwork net) {
		Parallel.forEach(net.layers, true, (layer) -> {
			Parallel.forEach(layer, true, (neuron) -> {
				switch (neuron.getNType()) {
				case Hidden:
					if (neuron.getParent().getLayerID() == 0 && net.numLayers() == 2) {
						neuron.getParent().removeNeuron(neuron);
						neuron.setParent(net.getLayer(0));
						neuron.setNType(neuron.getParent().getType());
					}
					if (neuron.getParent().getLayerID() == net.numLayers() - 1 && net.numLayers() == 2) {
						neuron.getParent().removeNeuron(neuron);
						neuron.setParent(net.getLayer(net.numLayers() - 1));
						neuron.setNType(neuron.getParent().getType());
					}
					if ((neuron.getParent().getLayerID() == net.numLayers() - 1 || neuron.getParent().getLayerID() == 0)
							&& net.numLayers() > 2) {
						neuron.getParent().removeNeuron(neuron);
						BaseLayer next = null;
						for (int i = 1; i < net.numLayers(); i++)
							if (net.getLayer(i).getType() == NeuronType.Hidden) {
								next = net.getLayer(i);
								break;
							}
						if (next == null) {
							NeuralNetEditor.putLayer(net, 1, BaseLayer.empty(NeuronType.Hidden, neuron.getGen()));
							net.getLayer(1).addNeuron(neuron);
							neuron.getParent().removeNeuron(neuron);
							neuron.setParent(net.getLayer(1));
						} else {
							next.addNeuron(neuron);
							neuron.getParent().removeNeuron(neuron);
							neuron.setParent(next);
						}
						neuron.setNType(neuron.getParent().getType());
					}
					break;
				case IO:
					if ((neuron.getParent().getLayerID() != 0 || neuron.getParent().getLayerID() != net.numLayers() - 1)
							&& net.numLayers() > 2) {
						neuron.setNType(neuron.getParent().getType());
					}
					break;
				case Input:
					if (neuron.getParent().getLayerID() != 0 && net.numLayers() > 1) {
						neuron.getParent().removeNeuron(neuron);
						if (net.getLayer(0).getType() != NeuronType.Input) {
							NeuralNetEditor.putLayer(net, 0, BaseLayer.empty(NeuronType.Input, neuron.getGen()));
						}
						neuron.setParent(net.getLayer(0));
						neuron.setNType(neuron.getParent().getType());
					}
					break;
				case LSTM:
					break;
				case Output:
					if (neuron.getParent().getLayerID() != net.numLayers() - 1 && net.numLayers() > 1) {
						neuron.getParent().removeNeuron(neuron);
						if (net.getLayer(net.numLayers() - 1).getType() != NeuronType.Output) {
							NeuralNetEditor.putLayer(net, net.numLayers() - 1,
									BaseLayer.empty(NeuronType.Output, neuron.getGen()));
						}
						neuron.setParent(net.getLayer(net.numLayers() - 1));
						neuron.setNType(neuron.getParent().getType());
					}
					break;
				case Reccurent:
					break;
				case Unknown:
					break;
				}
			});
		});
	}
	
	/**
	 * Convert double array set to data for net
	 * @param net
	 * @param set
	 */
	public static void includeTrainDataArrayToNet(BaseNeuralNetwork net, double[][][] set)
	{
		if(set[0][0][2] != 1)
		{
			Log.error("Incorrect data!");
			return;
		}
		if(NetIdentify.toTypeInt((int) set[0][0][0]) == net.networkType && NetIdentify.toGenInt((int) set[0][0][1]) == net.networkGen)
		{
			if(set.length - 2 != net.numLayers())
			{
				Log.error("This set doesn't fit the network!");
				return;
			}
			Parallel.makeFor(0, net.numLayers(), (i) -> {
				Parallel.makeFor(0, net.numLayers(), (j) -> {
					net.getNeuron(i, j).delta = set[1 + i][j][2];
				});
			});
			final int index = 1 + net.numLayers();
			Parallel.makeFor(0, net.weights.size(), (i) -> {
				net.weights.put(net.getNeuron((int)set[index][i][0], (int)set[index][i][1]), 
						net.getNeuron((int) set[index][i][0], (int)set[index][i][1]), set[index][i][4]);
			});
		}else
			Log.error("Incorrect data!");
	}
	
	/**
	 * Used to convert a network into a type that can train native code
	 * @param net
	 * @return
	 */
	public static double[][][] convertToArrayForTrain(BaseNeuralNetwork net)
	{
		double[][][] set = new double[2 + net.numLayers()][][];
		set[0][0] = new double[3];
		set[0][0][0] = NetIdentify.toIntType(net.networkType);
		set[0][0][1] = NetIdentify.toIntGen(net.networkGen);
		set[0][0][2] = 1;//0 for exec mode. 1 - for train
		if(net.networkGen == NeuralGen.Default)
		{
			for (int i = 0; i < net.numLayers(); i++)
			{
				set[1 + i] = new double[net.layers.get(i).length()][3];
			}
			Parallel.forEach(net.layers, false, (l) -> {
				final int i = net.layers.indexOf(l);
				Parallel.forEach(l, false, (n) -> {
					final int j = l.indexOf(n);
					set[1 + i][j][0] = NetIdentify.toIntActivation(n.activationType);
					set[1 + i][j][1] = n.createdValue;//createdValue
					set[1 + i][j][2] = n.delta;
				});
			});
			final int index = 1 + net.numLayers();
			set[1 + net.numLayers()] = new double[net.weights.size()][5];
			int[] i = new int[1];
			Parallel.forEach(net.weights.cellSet(), false, (c) -> {
				set[index][i[0]][0] = c.getRowKey().getLayerID();
				set[index][i[0]][1] = c.getRowKey().getLayerNID();
				set[index][i[0]][2] = c.getColumnKey().getLayerID();
				set[index][i[0]][3] = c.getColumnKey().getLayerNID();
				set[index][i[0]][4] = net.weights.get(c.getRowKey(), c.getColumnKey());
				i[0]++;
			});
		}else
		{
			//......
		}
		return set;
	}
	
	/**
	 * Used to convert a network into a type that can execute native code
	 * @param net
	 * @return
	 */
	public static double[][][] convertToArrayForExec(BaseNeuralNetwork net)
	{
		double[][][] set = new double[2 + net.numLayers()][][];
		set[0][0] = new double[3];
		set[0][0][0] = NetIdentify.toIntType(net.networkType);
		set[0][0][1] = NetIdentify.toIntGen(net.networkGen);
		set[0][0][2] = 0;//0 for exec mode. 1 - for train
		if(net.networkGen == NeuralGen.Default)
		{
			for (int i = 0; i < net.numLayers(); i++)
			{
				set[1 + i] = new double[net.layers.get(i).length()][5];
			}
			Parallel.forEach(net.layers, false, (l) -> {
				final int i = net.layers.indexOf(l);
				Parallel.forEach(l, false, (n) -> {
					final int j = l.indexOf(n);
					set[1 + i][j][0] = NetIdentify.toIntActivation(n.activationType);
					set[1 + i][j][1] = ((DefNeuron)n).steepness();
					set[1 + i][j][2] = ((DefNeuron)n).bias();
					set[1 + i][j][3] = 0;//obtainedValue
					set[1 + i][j][4] = 0;//createdValue
				});
			});
			final int index = 1 + net.numLayers();
			set[1 + net.numLayers()] = new double[net.weights.size()][5];
			int[] i = new int[1];
			Parallel.forEach(net.weights.cellSet(), false, (c) -> {
				set[index][i[0]][0] = c.getRowKey().getLayerID();
				set[index][i[0]][1] = c.getRowKey().getLayerNID();
				set[index][i[0]][2] = c.getColumnKey().getLayerID();
				set[index][i[0]][3] = c.getColumnKey().getLayerNID();
				set[index][i[0]][4] = net.weights.get(c.getRowKey(), c.getColumnKey());
				i[0]++;
			});
		}else
		{
			//......
		}
		return set;
	}

	private static class NetSorter implements Comparator<BaseNeuron> {
		@Override
		public int compare(BaseNeuron arg0, BaseNeuron arg1) {
			switch (arg0.getNType()) {
			case Hidden:
				switch (arg1.getNType()) {
				case Hidden:
					return 0;
				case IO:
					return 1;
				case Input:
					return 1;
				case Output:
					return -1;
				case Reccurent:
					return 0;
				case LSTM:
					return 0;
				case Unknown:
					return -1;
				}
				break;
			case IO:
				switch (arg1.getNType()) {
				case Hidden:
					return -1;
				case IO:
					return 0;
				case Input:
					return 1;
				case Output:
					return -1;
				case Reccurent:
					return -1;
				case LSTM:
					return -1;
				case Unknown:
					return -1;
				}
				break;
			case Input:
				switch (arg1.getNType()) {
				case Hidden:
					return -1;
				case IO:
					return -1;
				case Input:
					return 0;
				case Output:
					return -1;
				case Reccurent:
					return -1;
				case LSTM:
					return -1;
				case Unknown:
					return -1;
				}
				break;
			case Output:
				switch (arg1.getNType()) {
				case Hidden:
					return 1;
				case IO:
					return 1;
				case Input:
					return 1;
				case Output:
					return 0;
				case Reccurent:
					return 1;
				case LSTM:
					return 1;
				case Unknown:
					return -1;
				}
				break;
			case Reccurent:
				switch (arg1.getNType()) {
				case Hidden:
					return 0;
				case IO:
					return 1;
				case Input:
					return 1;
				case Output:
					return -1;
				case Reccurent:
					return 0;
				case LSTM:
					return 0;
				case Unknown:
					return -1;
				}
				break;
			case Unknown:
				switch (arg1.getNType()) {
				case Hidden:
					return 1;
				case IO:
					return 1;
				case Input:
					return 1;
				case Output:
					return 1;
				case Reccurent:
					return 1;
				case LSTM:
					return 1;
				case Unknown:
					return 0;
				}
				break;
			case LSTM:
				switch (arg1.getNType()) {
				case Hidden:
					return 0;
				case IO:
					return 1;
				case Input:
					return 1;
				case Output:
					return -1;
				case Reccurent:
					return 0;
				case LSTM:
					return 0;
				case Unknown:
					return -1;
				}
				break;
			}
			return -1;
		}
	}

	public static boolean cycleChecker(DefNeuralNetwork net) {
		for (int i = 0; i < net.getOutputSize(); i++) {
			if (!advDFS(net.getLayer(net.numLayers() - 1).get(i)))
				return false;
			checked = HashBasedTable.create();
		}
		return true;
	}

	private static boolean advDFS(BaseNeuron n) {
		try {
			if (checked.get(n.getLayerID(), n.getLayerNID()))
				return false;
		} catch (NullPointerException e) {
		}
		checked.put(n.getLayerID(), n.getLayerNID(), true);
		for (int i = 0; i < n.setters().size(); i++)
			if (!advDFS(n.setters().get(i)))
				return false;
		checked.put(n.getLayerID(), n.getLayerNID(), false);
		return true;
	}

	public static boolean fullConnectivityCheck(DefNeuralNetwork net) {
		checked = HashBasedTable.create();
		if (DFS(net.getNeuron(0, 0)) == net.neuronCount())
			return true;
		else
			return false;
	}

	private static int DFS(BaseNeuron n) {
		try {
			if (checked.get(n.getLayerID(), n.getLayerNID()))
				return 0;
		} catch (NullPointerException e) {
		}
		checked.put(n.getLayerID(), n.getLayerNID(), true);
		int sum = 1;
		for (BaseNeuron s : n.setters)
			sum += DFS(s);
		for (BaseNeuron g : n.getters)
			sum += DFS(g);
		return sum;
	}

}
