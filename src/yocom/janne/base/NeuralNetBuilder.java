package yocom.janne.base;

import yocom.janne.action.ConnectivityAction;
import yocom.janne.calc.Parallel;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuralNetworkType;
import yocom.janne.util.MathUtils;

public class NeuralNetBuilder
{
	private BaseNeuralNetwork build;
	private ConnectivityAction connectAction;
	private connectivityType connectT = connectivityType.NOT_CONNECT;

	public static enum connectivityType
	{
		FULL_CONNECTIVITY, CUSTOM_CONNECTIVITY, NOT_CONNECT
	}

	public NeuralNetBuilder(NeuralGen type, NeuralNetworkType typeNet)
	{
		switch (type)
		{
			case Default:
				build = new DefNeuralNetwork();
				build.networkType = typeNet;
				build.init();
				break;
			case Spike:
				//TODO STUB
				break;
			case Unknown:
				break;
		}
	}

	public NeuralNetBuilder addLayer(BaseLayer l)
	{
		if (l.ngen == build.networkGen)
		{
			if (build.numLayers() > 0)
			{
				NeuralNetEditor.putLayer(build, l);
				switch (connectT)
				{
					case CUSTOM_CONNECTIVITY:
						connectAction.connect(build, build.getLayer(build.numLayers() - 2),
								build.getLayer(build.numLayers() - 1));
						break;
					case FULL_CONNECTIVITY:
						Parallel.forEach(build.getLayer(build.numLayers() - 2), false, (n) -> {
							Parallel.forEach(build.getLayer(build.numLayers() - 1), false, (n2) -> {
								NeuralNetEditor.createSynapse(build, n, n2, true);
							});
						});
						break;
					case NOT_CONNECT:
						break;
				}
			} else
				NeuralNetEditor.putLayer(build, 0, l);
			build.refreshLayers();
		} else
			Log.error("This layer generation doesn't match the network generation!");
		return this;
	}
	
	/**
	 * Connects all neurons of the last layer with each other(impossible to connect to itself). Suitable for building a Hopfield network or other occasions.
	 * @param id - id of the layer
	 * @param initWeights - generate weights or not
	 */
	public NeuralNetBuilder connectNeuronsOfLastLayer(boolean initWeights)
	{
		BaseLayer l = build.getLayer(build.numLayers() - 1);
		if(initWeights)
			Parallel.makeFor(0, l.length(), (i) -> {
				Parallel.makeFor(0, l.length(), (j) -> {
					if(!build.synapseExist(0, j, 0, i) && i != j)
						build.weights.put(build.getNeuron(0, i), build.getNeuron(0, j), MathUtils.randomDouble(-0.5, 0.5));
				});
			});
		else
			Parallel.makeFor(0, l.length(), (i) -> {
				Parallel.makeFor(0, l.length(), (j) -> {
					if(!build.synapseExist(0, j, 0, i) && i != j)
						build.weights.put(build.getNeuron(0, i), build.getNeuron(0, j), 0D);
				});
			});
		return this;
	}
	
	/**
	 * Connects all neurons of the selected layer with each other(impossible to connect to itself). Suitable for building a Hopfield network or other occasions.
	 * @param id - id of the layer
	 * @param initWeights - generate weights or not
	 */
	public NeuralNetBuilder connectNeuronsOfLayer(int id, boolean initWeights)
	{
		BaseLayer l = build.getLayer(id);
		if(initWeights)
			Parallel.makeFor(0, l.length(), (i) -> {
				Parallel.makeFor(0, l.length(), (j) -> {
					if(!build.synapseExist(0, j, 0, i) && i != j)
						build.weights.put(build.getNeuron(0, i), build.getNeuron(0, j), MathUtils.randomDouble(-0.5, 0.5));
				});
			});
		else
			Parallel.makeFor(0, l.length(), (i) -> {
				Parallel.makeFor(0, l.length(), (j) -> {
					if(!build.synapseExist(0, j, 0, i) && i != j)
						build.weights.put(build.getNeuron(0, i), build.getNeuron(0, j), 0D);
				});
			});
		return this;
	}

	/**
	 * Connects the layer(getter) with many other layers(setters) using the
	 * specified connection rules.
	 */
	public NeuralNetBuilder connectCustomLayersV1(ConnectivityAction action, int layerID1, int... layerID0)
	{
		for (int i = 0; i < layerID0.length; i++)
		{
			action.connect(build, build.getLayer(layerID0[i]), build.getLayer(layerID1));
		}

		return this;
	}

	/**
	 * Connects the layer(setter) with many other layers(getters) using the
	 * specified connection rules.
	 */
	public NeuralNetBuilder connectCustomLayersV0(ConnectivityAction action, int layerID0, int... layerID1)
	{
		for (int i = 0; i < layerID1.length; i++)
		{
			action.connect(build, build.getLayer(layerID0), build.getLayer(layerID1[i]));
		}

		return this;
	}

	public NeuralNetBuilder connectCustomLayers(ConnectivityAction action, int layerID0, int layerID1)
	{
		action.connect(build, build.getLayer(layerID0), build.getLayer(layerID1));
		return this;
	}

	public NeuralNetBuilder fullConnectivityToNext()
	{
		connectT = connectivityType.FULL_CONNECTIVITY;
		return this;
	}

	public NeuralNetBuilder noConnectivityToNext()
	{
		connectT = connectivityType.NOT_CONNECT;
		return this;
	}

	public NeuralNetBuilder customConnectivityToNext(ConnectivityAction action)
	{
		connectT = connectivityType.CUSTOM_CONNECTIVITY;
		connectAction = action;
		return this;
	}

	public BaseNeuralNetwork getNet()
	{
		return build;
	}

	public void destroy()
	{
		connectAction = null;
		build = null;
	}
}
