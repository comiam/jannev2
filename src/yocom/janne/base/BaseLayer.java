package yocom.janne.base;

import java.util.ArrayList;

import yocom.janne.calc.Parallel;
import yocom.janne.io.Log;
import yocom.janne.neurons.DefNeuron;
import yocom.janne.types.ActivationType;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuralNetworkType;
import yocom.janne.types.NeuronType;

public class BaseLayer extends ArrayList<BaseNeuron>
{
	private static final long serialVersionUID = -3898651466257898071L;
	protected BaseNeuralNetwork parent;
	protected NeuronType mainType = NeuronType.Unknown;
	protected NeuralGen ngen = NeuralGen.Unknown;
	private boolean onAdding = false;

	public static BaseLayer empty(NeuronType type, NeuralGen gen)
	{
		BaseLayer result = new BaseLayer();
		result.ngen = gen;
		result.mainType = type;
		return result;
	}

	public static BaseLayer create(int size, ActivationType atype, NeuronType type, NeuralGen gen, double steepness,
			double shift)
	{
		BaseLayer result = new BaseLayer();
		result.ngen = gen;
		result.mainType = type;
		switch (gen)
		{
			case Default:
				Parallel.makeFor(0, size, (i) -> {
					result.addNeuron(new DefNeuron(result, steepness, shift, type, atype));
				});
				break;
			case Spike:
				// FIXME
				return null;
			case Unknown:
				Log.error("Not determined the generation of a neuron!");
				return null;
		}
		return result;
	}

	public static BaseLayer create(int size, ActivationType atype, NeuralGen gen)
	{
		return create(size, atype, NeuronType.Unknown, gen, BaseNeuron.DEFAULT_ACTIVATION_STEEPNESS,
				BaseNeuron.DEFAULT_ACTIVATION_BIAS);
	}

	public static BaseLayer create(int size, ActivationType atype, NeuronType type, NeuralGen gen)
	{
		return create(size, atype, type, gen, BaseNeuron.DEFAULT_ACTIVATION_STEEPNESS,
				BaseNeuron.DEFAULT_ACTIVATION_BIAS);
	}

	public static BaseLayer untyped(int size, NeuralGen gen, ActivationType atype)
	{
		return untyped(size, gen, atype, BaseNeuron.DEFAULT_ACTIVATION_STEEPNESS, BaseNeuron.DEFAULT_ACTIVATION_BIAS);
	}

	public static BaseLayer untyped(int size, NeuralGen gen, ActivationType atype, double steepness, double shift)
	{
		BaseLayer result = new BaseLayer();
		result.ngen = gen;
		switch (gen)
		{
			case Default:
				Parallel.makeFor(0, size, (i) -> {
					result.addNeuron(new DefNeuron(result, steepness, shift, NeuronType.Unknown, atype));
				});
				break;
			case Spike:
				// FIXME
				return null;
			case Unknown:
				Log.error("Not determined the generation of a neuron!");
				return null;
		}
		return result;
	}

	@Override
	@Deprecated
	public synchronized boolean add(BaseNeuron neuron)
	{
		if (onAdding)
		{
			onAdding = false;
			neuron.setParent(this);
			return super.add(neuron);
		} else
		{
			Log.error("This method isn't used in the BaseLayer class! Neuron hasn't been added!");
			return false;
		}
	}

	@Override
	@Deprecated
	public synchronized void add(int index, BaseNeuron neuron)
	{
		if (onAdding)
		{
			onAdding = false;
			neuron.setParent(this);
			super.add(neuron);
		} else
			Log.error("This method isn't used in the BaseLayer class! Neuron hasn't been added!");
	}

	public boolean addNeuron(BaseNeuron neuron)
	{
		if (parent != null && parent.networkGen != neuron.neuronGen)
		{
			Log.error("This neuron is not suitable for this neural network!");
			return false;
		}
		if (neuron.neuronType != mainType && mainType != NeuronType.Unknown)
		{
			Log.error("This neuron is not suitable for this neural network!");
			return false;
		} else if (mainType == NeuronType.Unknown)
			mainType = neuron.neuronType;
		switch (ngen)
		{
			case Default:
				if (neuron instanceof DefNeuron)
				{
					onAdding = true;
					return add(neuron);
				}
				return false;
			case Spike:
				return false;
			case Unknown:
				Log.error("Not determined the generation of a neuron!");
				return false;
		}
		return false;
	}

	public void addNeuron(int LNID, BaseNeuron neuron)
	{
		if (parent != null && parent.networkGen != neuron.neuronGen)
		{
			Log.error("This neuron is not suitable for this neural network!");
			return;
		}
		if (neuron.neuronType != mainType)
		{
			Log.error("This neuron is not suitable for this neural network!");
			return;
		}
		switch (ngen)
		{
			case Default:
				if (neuron instanceof DefNeuron)
				{
					onAdding = true;
					super.add(LNID, neuron);
				}
			case Spike:
				break;
			case Unknown:
				Log.error("Not determined the generation of a neuron!");
				break;
		}
	}

	public void readInput()
	{
		if (parent.networkType != NeuralNetworkType.HopfieldNN)
		{
			Parallel.forEach(this, false, (n) -> {
				if (!n.setters.isEmpty())
					Parallel.forEach(n.setters, false, (ns) -> {
						n.obtainedValue += ns.createdValue * parent.getWeightSynapse(ns, n);
					});
			});
		} else
		{
			double[] weight = new double[1];
			if(get(0).createdValue == 0)
				Parallel.makeFor(0, length(), (i) -> {
					get(i).obtainedValue = 0;
					Parallel.makeFor(0, length(), (j) -> {
						if (i != j)
							if (!Double.isNaN(weight[0] = parent.getWeightSynapse(0, i, 0, j)))
								get(i).obtainedValue += weight[0] * get(j).obtainedValue;
							else
								get(i).obtainedValue += parent.getWeightSynapse(0, j, 0, i) * get(j).obtainedValue;
					});
				});
			else
				Parallel.makeFor(0, length(), (i) -> {
					get(i).obtainedValue = 0;
					Parallel.makeFor(0, length(), (j) -> {
						if (i != j)
							if (!Double.isNaN(weight[0] = parent.getWeightSynapse(0, i, 0, j)))
								get(i).obtainedValue += weight[0] * get(j).createdValue;
							else
								get(i).obtainedValue += parent.getWeightSynapse(0, j, 0, i) * get(j).createdValue;
					});
				});
		}
	}

	public void calcInput()
	{
		Parallel.forEach(this, false, (n) -> {
			n.activate();
		});
	}

	public void removeNeuron(BaseNeuron bn)
	{
		if (contains(bn))
			remove(bn);
	}

	public void removeNeuron(int NID)
	{
		if (length() > NID && NID >= 0)
			remove(NID);
	}

	public int beginNetIndex()
	{
		if (parent == null)
			return -1;
		else
		{
			int begin = 0;
			for (int i = 0; i < parent.numLayers(); i++)
			{
				if (!parent.getLayer(i).equals(this))
					begin += parent.getLayer(i).length();
				else
					break;
			}

			return begin;
		}
	}

	public void setType(NeuronType type)
	{
		this.mainType = type;
		Parallel.forEach(this, true, (neuron) -> {
			neuron.setNType(type);
		});
	}

	public int length()
	{
		return this.size();
	}

	public void setParent(BaseNeuralNetwork parent)
	{
		if (ngen != NeuralGen.Unknown && parent.getGen() != ngen)
		{
			Log.error("This parent type doesn't match the layer type!");
			return;
		}
		this.parent = parent;
	}

	public NeuronType getType()
	{
		return mainType;
	}

	public int getLayerID()
	{
		return parent == null ? -1 : parent.layers.indexOf(this);
	}

	public BaseNeuralNetwork getParent()
	{
		return parent;
	}
}
