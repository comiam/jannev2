package yocom.janne.neurons;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import yocom.janne.base.BaseLayer;
import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.BaseNeuron;
import yocom.janne.calc.Parallel;
import yocom.janne.types.ActivationType;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuronType;

public class DefNeuron extends BaseNeuron
{
	private static final long serialVersionUID = 8833783656484004852L;
	private double steepness = DEFAULT_ACTIVATION_STEEPNESS;
	private double bias = DEFAULT_ACTIVATION_BIAS;
	private boolean updated = false;
	
	public DefNeuron()
	{
		this(null, DEFAULT_ACTIVATION_STEEPNESS, DEFAULT_ACTIVATION_BIAS, NeuronType.Unknown, ActivationType.Sigmoid);
	}
	
	public DefNeuron(Map<String, String> map)
	{
		this.neuronGen = NeuralGen.Default;
		implNecessaryData(map);
	}

	public DefNeuron(BaseLayer parent, double steepness, double shift, ActivationType atype)
	{
		this(parent, steepness, shift, NeuronType.Unknown, atype);
	}

	public DefNeuron(double steepness, double shift, ActivationType atype)
	{
		this(null, steepness, shift, NeuronType.Unknown, atype);
	}

	public DefNeuron(double steepness, double shift, NeuronType type, ActivationType atype)
	{
		this(null, steepness, shift, type, atype);
	}

	public DefNeuron(BaseLayer parent, double steepness, double shift, NeuronType ntype,
			ActivationType atype)
	{
		setParent(parent);
		this.neuronGen = NeuralGen.Default;
		this.neuronType = ntype;
		this.steepness = steepness;
		this.activationType = atype;
		this.bias = shift;
	}
	
	@Override
	public void activate()
	{
		createdValue = ActivationType.engine(activationType, obtainedValue + bias, steepness);
	}

	@Override
	public void update()
	{
		if (getters.size() == 0 && setters.size() == 0)
			neuronType = NeuronType.Unknown;
		else if (getters.size() > 0 && setters.size() == 0)
			neuronType = NeuronType.Input;
		else if (getters.size() == 0 && setters.size() > 0)
			neuronType = NeuronType.Output;
		else if (getters.size() > 0 && setters.size() > 0 && neuronType != NeuronType.IO && neuronType != NeuronType.Reccurent
				&& neuronType != NeuronType.LSTM)
			neuronType = NeuronType.Hidden;
	}

	@Override
	public void calculateSettersSum(BaseNeuralNetwork bn)
	{
		if(setters != null)
		{
			obtainedValue = 0;
			Parallel.forEach(setters, false, (neuron) -> {
				obtainedValue += neuron.obtained() * bn.getWeightSynapse(neuron, this);
			});
		}
	}
	
	public double steepness()
	{
		return steepness;
	}
	
	public double bias()
	{
		return bias;
	}
	
	public boolean updated()
	{
		return updated;
	}
	
	public void setBias(double shift)
	{
		this.bias = shift;
	}
	
	public void setSteepness(double steepness)
	{
		this.steepness = steepness;
	}
	
	public void setUpdated(boolean updated)
	{
		this.updated = updated;
	}

	@Override
	public Map<String, String> getNecessaryData()
	{
		Map<String, String> map = new HashMap<String, String>();
		map.put("activationType", "");
		map.put("neuronType", "");
		map.put("bias", "");
		map.put("steepness", "");
		map.put("getters", "");
		map.put("setters", "");
		
		return map;
	}
	
	@Override
	public void implNecessaryData(Map<String, String> map)
	{
		activationType = ActivationType.valueOf(map.get("activationType"));
		neuronType = NeuronType.valueOf(map.get("neuronType"));
		bias = Double.parseDouble(map.get("bias"));
		steepness = Double.parseDouble(map.get("steepness"));
	}

	@Override
	public void initGettersAndSetters(String getters, String setters)
	{
		final int[] intPair = new int[1];
		intPair[0] = -1;
		final BaseNeuralNetwork net = this.layer.getParent();
		
		if(!getters.isEmpty())
			Pattern.compile(" ").splitAsStream(getters).mapToInt((str) -> {
				if(!str.isEmpty())
					return Integer.parseInt(str);
				else
					return -1;
			}).forEachOrdered((i) -> {
				if(intPair[0] == -1 && i != -1)
					intPair[0] = i;
				else
				{
					this.getters.add(net.getNeuron(intPair[0], i));
					intPair[0] = -1;
				}
			});
		if(!setters.isEmpty())
			Pattern.compile(" ").splitAsStream(setters).mapToInt((str) -> {
				if(!str.isEmpty())
					return Integer.parseInt(str);
				else
					return -1;
			}).forEachOrdered((i) -> {
				if(intPair[0] == -1 && i != -1)
					intPair[0] = i;
				else
				{
					this.setters.add(net.getNeuron(intPair[0], i));
					intPair[0] = -1;
				}
			});
	}
}
