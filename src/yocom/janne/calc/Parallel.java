package yocom.janne.calc;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;

public class Parallel
{
	public static void makeFor(int begin, int end, IntConsumer lambda)
	{
		if(Hardware.rationallyUseMT())
			IntStream.range(begin, end).parallel().forEachOrdered(lambda);
		else
			for(int i = begin;i < end;i++)
				lambda.accept(i);
	}
	
	public static <T> void forEach(List<T> a, boolean unordered, Consumer<T> lambda)
	{
		if(Hardware.rationallyUseMT())
		{
			if (unordered)
				a.parallelStream().unordered().forEach(lambda);
			else
				a.parallelStream().forEachOrdered(lambda);
		}else 
			for(T t : a)
				lambda.accept(t);
	}
	
	public static <T> void forEach(Set<T> a, boolean unordered, Consumer<T> lambda)
	{
		if(Hardware.rationallyUseMT())
		{
			if (unordered)
				a.parallelStream().unordered().forEach(lambda);
			else
				a.parallelStream().forEachOrdered(lambda);
		}else 
			for(T t : a)
				lambda.accept(t);
	}
	
	public static <T> void forEach(T[] a, boolean unordered, Consumer<T> lambda)
	{
		if(Hardware.rationallyUseMT())
		{
			if (unordered)
				Arrays.stream(a).parallel().unordered().forEach(lambda);
			else
				Arrays.stream(a).parallel().forEachOrdered(lambda);
		}else
			for(T t : a)
				lambda.accept(t);
	}
}
