package yocom.janne.calc;

import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.NeuralNetProgram;

public class NativeExecutor
{
	/**
	 * It is used to check the possibility of calculations in native code
	 */
	public static boolean nativeAvailable()
	{
		return false;
	}
	
	public static double[] execute(BaseNeuralNetwork net, NeuralNetProgram prog, double...inputs)
	{
		return null;
	}
}
