package yocom.janne.calc;

import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.NeuralNetProgram;

public class GPUExecutor
{
	/**
	 * Used to check the availability of GPU for accelerated computing
	 */
	public static boolean GPUAvailable()
	{
		return false;
	}
	
	public static double[] execute(BaseNeuralNetwork net, NeuralNetProgram prog, double...inputs)
	{
		return null;
	}
}
