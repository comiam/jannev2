package yocom.janne.calc;

public class Hardware
{
	private static final int availableCores = Runtime.getRuntime().availableProcessors();
	private static final boolean supportGPUAcceleration = supportGPU();

	public static int getNumCores()
	{
		return availableCores;
	}

	/**
	 * @return Может ли пк поддерживать мультипоточность.
	 */
	public static boolean rationallyUseMT()
	{
		return availableCores != 1;
	}

	public static boolean supportGPUAcceleration()
	{
		return supportGPUAcceleration;
	}

	private static boolean supportGPU()
	{
		return false;
	}
}
