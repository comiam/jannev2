package yocom.janne.util;

import yocom.janne.base.BaseNeuron;
import yocom.janne.neurons.DefNeuron;

public class MathUtils
{
	public static double randomDouble(double min, double max)
	{
		return min + (max - min) * Math.random();
	}

	public static double countDerivative(BaseNeuron n)
	{
		switch (n.getAType())
		{
			case Sigmoid:
				return sigmoidDerivative(n.created());
			case HyperbolicTangent:
				return tanghDerivative(n.created());
			case Cos:
				return cosDerivative(n.obtained());
			case Gaussian:
				return gaussianDerivative(n.obtained());
			case Linear:
				return 1;
			case PReLU:
				return preluDerivative((DefNeuron) n, n.obtained());
			case ReLU:
				return reluDerivative(n.obtained());
			case Sin:
				return sinDerivative(n.obtained());
			case SoftPlus:
				return softPlusDerivative(n.obtained());
			case Threshold:
				return 0;
			case Unknown:
				return -1;
			default:
				return -1;
		}
	}

	private static double softPlusDerivative(double x)
	{
		return 1 / (1 + Math.exp(-x));
	}

	private static double reluDerivative(double x)
	{
		if (x < 0)
			return 0;
		else
			return 1;
	}

	private static double preluDerivative(DefNeuron n, double x)
	{
		if (x < 0)
			return n.steepness();
		else
			return 1;
	}

	private static double gaussianDerivative(double x)
	{
		return -2 * x * Math.exp(-x * x);
	}

	private static double sinDerivative(double x)
	{
		return Math.cos(x);
	}

	private static double cosDerivative(double x)
	{
		return -Math.sin(x);
	}

	private static double sigmoidDerivative(double x)
	{
		return (1 - x) * x;
	}

	private static double tanghDerivative(double x)
	{
		return 1 - (x * x);
	}

}
