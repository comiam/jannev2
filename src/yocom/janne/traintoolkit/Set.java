package yocom.janne.traintoolkit;

import java.util.ArrayList;

public class Set {

	private ArrayList<Double> input = new ArrayList<Double>();
	private ArrayList<Double> output = new ArrayList<Double>();

	public Set addToInput(int neuronID, double value) {
		input.add(neuronID, value);
		return this;
	}

	public Set addToInput(double... value) {
		for (int i = 0; i < value.length; i++)
			input.add(input.size(), value[i]);
		return this;
	}

	public Set addToOutput(int neuronID, double value) {
		output.add(neuronID, value);
		return this;
	}
	public Set addToOutput(double... value) {
		for (int i = 0; i < value.length; i++)
			output.add(output.size(), value[i]);
		return this;
	}

	public double getFromInput(int neuronID) {
		return input.get(neuronID);
	}

	public double getFromOutput(int neuronID) {
		return output.get(neuronID);
	}

	public ArrayList<Double> getInputs() {
		// TODO Auto-generated method stub
		return input;
	}
	public double[] getInputsD() {
		// TODO Auto-generated method stub
		double[] arr = new double[input.size()];
		for (int i = 0; i < arr.length; i++)
			arr[i] = input.get(i);
		return arr;
	}


	public ArrayList<Double> getOutputs() {
		// TODO Auto-generated method stub
		return output;
	}
	public double[] getOutputsD() {
		// TODO Auto-generated method stub
		double[] arr = new double[output.size()];
		for (int i = 0; i < arr.length; i++)
			arr[i] = output.get(i);
		return arr;
	}
}
