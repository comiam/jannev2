package yocom.janne.traintoolkit;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import com.google.common.collect.HashBasedTable;

import yocom.janne.base.BaseNeuralNetwork;
import yocom.janne.base.BaseNeuron;
import yocom.janne.base.NeuralNetProgram;
import yocom.janne.base.NeuralNetCreator;
import yocom.janne.base.NeuralNetEditor;
import yocom.janne.base.NeuralNetExecutor;
import yocom.janne.calc.Parallel;
import yocom.janne.io.FastScanner;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.types.NeuronType;
import yocom.janne.util.MathUtils;

public class NetTrainer
{
	private static double a;
	private static double E;
	private static BaseNeuralNetwork net;
	private static Epoch e;
	private static HashBasedTable<BaseNeuron, BaseNeuron, Double> prWe = HashBasedTable.create();
	private static boolean training = false;
	private static boolean breaked = false;
	private static boolean logging = true;
	private static boolean fullDebugError = false;
	private static double currentError = 0;

	private static double sum = 0;
	private static boolean makeCrashLog = false;

	public static void setLogging(boolean training)
	{
		logging = training;
	}

	public static void setFullErrorLogging(boolean training)
	{
		fullDebugError = training;
	}

	public static boolean isTraining()
	{
		return training;
	}

	public static void enableCrashLog()
	{
		NetTrainer.makeCrashLog = true;
	}

	public static void disableCrashLog()
	{
		NetTrainer.makeCrashLog = false;
	}

	public static double trainPerceptron(BaseNeuralNetwork netw, Epoch ep, double learningSpeed, double momentum,
			double prefferedError, double iteratLogns, int formatSize)
	{
		if (netw == null || ep == null || iteratLogns <= 0 || Double.isNaN(prefferedError) || Double.isNaN(momentum)
				|| Double.isNaN(learningSpeed) || formatSize <= 0)
		{
			Log.error("Incorrect args!");
			return netw.currentError();
		}

		if (ep.get(0).getInputs().size() > netw.getLayer(0).size())
		{
			Log.error("Count of input data more than input neurons!");
			return netw.currentError();
		}
		if (ep.get(0).getInputs().size() < netw.getLayer(0).size())
		{
			Log.error("Count of input data less than input neurons!");
			return netw.currentError();
		}
		if (ep.get(0).getOutputs().size() > netw.getLayer(netw.numLayers() - 1).size())
		{
			Log.error("Count of output data more than output neurons!");
			return netw.currentError();
		}
		if (ep.get(0).getOutputs().size() < netw.getLayer(netw.numLayers() - 1).size())
		{
			Log.error("Count of output data less than output neurons!");
			return netw.currentError();
		}

		E = learningSpeed;
		a = momentum;
		net = netw;
		e = ep;
		currentError = 0;
		BaseNeuron current;
		NeuralNetExecutor.setLogging(false);
		String format = "%." + formatSize + "f";
		training = true;
		NeuralNetProgram prog = NeuralNetCreator.getProgramForPerceptron(net);

		if (logging)
			Log.info("Training has begun.");
		for (int o = 0; o < iteratLogns; o++)
		{
			for (int k = 0; k < e.size(); k++)
			{
				currentError += NetError.MSE(net, e.get(k).getOutputsD(),
						NeuralNetExecutor.run(net, prog, e.get(k).getInputs()));

				for (int i = net.numLayers() - 1; i >= 0; i--)
					for (int j = net.getLayer(i).size() - 1; j >= 0; j--)
					{
						current = net.getLayer(i).get(j);
						if (current.getNType().equals(NeuronType.Output))
							countDelta(current, e.get(k).getFromOutput(j));
						else if (current.getNType() == NeuronType.Hidden)
							countDelta(current);
					}
				net.resetObtainedValues();
			}
			net.setCurrentError(currentError / e.size());
			currentError = 0;
			if (!fullDebugError && o % 100 == 0 && logging)
				Log.debug("Iteration " + o + ". Current error: " + String.format(format, net.currentError()));
			else if (fullDebugError)
				Log.debug("Iteration " + o + ". Current error: " + String.format(format, net.currentError()));

			if (net.currentError() <= prefferedError)
			{
				if (makeCrashLog)
					Log.createTrainingReport(net, prog, e, E, a, iteratLogns);
				if (logging)
					Log.debug("Iteration " + o + ". Current error: " + String.format(format, net.currentError()));
				Log.info("Training ended.");
				break;
			}
		}
		if (makeCrashLog)
			Log.createTrainingReport(net, prog, e, E, a, iteratLogns);

		training = false;
		NeuralNetExecutor.setLogging(true);
		return net.currentError();
	}

	public static double trainPerceptron(BaseNeuralNetwork netw, File ep, double learningSpeed, double momentum,
			double prefferedError, double iteratLogns, int formatSize)
	{
		if (netw == null || ep == null || iteratLogns <= 0 || Double.isNaN(prefferedError) || Double.isNaN(momentum)
				|| Double.isNaN(learningSpeed) || formatSize <= 0)
		{
			Log.error("Incorrect args!");
			return netw.currentError();
		}

		if (!ep.exists())
		{
			Log.error("Trainset file is missing!");
		}

		try (FastScanner scan = new FastScanner(new FileInputStream(ep)))
		{
			E = learningSpeed;
			a = momentum;
			net = netw;
			currentError = 0;
			BaseNeuron current;
			NeuralNetExecutor.setLogging(false);
			String format = "%." + formatSize + "f";
			training = true;
			NeuralNetProgram prog = NeuralNetCreator.getProgramForPerceptron(net);

			int setsCount = scan.nextInt();
			int countInput = scan.nextInt();
			int countOutput = scan.nextInt();

			double[] inputs = new double[countInput];
			double[] outputs = new double[countOutput];

			if (inputs.length > netw.getLayer(0).size())
			{
				Log.error("Count of input data more than input neurons!");
				return netw.currentError();
			}
			if (inputs.length < netw.getLayer(0).size())
			{
				Log.error("Count of input data less than input neurons!");
				return netw.currentError();
			}
			if (outputs.length > netw.getLayer(netw.numLayers() - 1).size())
			{
				Log.error("Count of output data more than output neurons!");
				return netw.currentError();
			}
			if (outputs.length < netw.getLayer(netw.numLayers() - 1).size())
			{
				Log.error("Count of output data less than output neurons!");
				return netw.currentError();
			}

			scan.mark((int) ep.length());

			if (logging)
				Log.info("Training has begun.");

			for (int o = 0; o < iteratLogns; o++)
			{
				for (int k = 0; k < setsCount; k++)
				{
					for (int j = 0; j < countInput; j++)
					{
						inputs[j] = scan.nextDouble();
					}
					for (int j = 0; j < countOutput; j++)
					{
						outputs[j] = scan.nextDouble();
					}

					currentError += NetError.MSE(net, outputs, NeuralNetExecutor.run(net, prog, inputs));

					for (int i = net.numLayers() - 1; i >= 0; i--)
						for (int j = net.getLayer(i).size() - 1; j >= 0; j--)
						{
							current = net.getLayer(i).get(j);
							if (current.getNType().equals(NeuronType.Output))
								countDelta(current, outputs[j]);
							else if (current.getNType() == NeuronType.Hidden)
								countDelta(current);
						}
					net.resetObtainedValues();
				}
				
				net.setCurrentError(currentError / setsCount);
				currentError = 0;
				if (!fullDebugError && o % 100 == 0 && logging)
					Log.debug("Iteration " + o + ". Current error: " + String.format(format, net.currentError()));
				else if (fullDebugError)
					Log.debug("Iteration " + o + ". Current error: " + String.format(format, net.currentError()));

				if (net.currentError() <= prefferedError)
				{
					if (makeCrashLog)
						Log.createTrainingReport(net, prog, ep, E, a, iteratLogns);
					if (logging)
						Log.debug("Iteration " + o + ". Current error: " + String.format(format, net.currentError()));
					Log.info("Training ended.");
					break;
				}
				
				scan.reset();
			}
			return net.currentError();
		} catch (Throwable e)
		{
			e.printStackTrace();
			Log.error("Error during learning net!");
			return -1;
		}
	}

	public static boolean trainHopfieldNN(DefNeuralNetwork net, double[][] vectorData)
	{
		if (vectorData.length == 0)
		{
			Log.error("Incorrect args!");
		}
		breaked = false;
		Parallel.makeFor(0, vectorData.length, (i) -> {
			if (net.neuronCount() != vectorData[i].length)
			{
				Log.error("Train vector " + i + " doesn't fit to the net!");
				breaked = true;
				return;
			}
		});
		if (breaked)
			return false;

		if (logging)
			Log.info("Training has begun.");
		double[] sum = new double[1];
		final int nc = net.neuronCount();
		Parallel.makeFor(0, nc, (i) -> {
			Parallel.makeFor(0, nc, (j) -> {
				if (i != j)
				{
					Parallel.makeFor(0, vectorData.length, (j2) -> {
						sum[0] += vectorData[j2][i] * vectorData[j2][j];
					});
					if (net.synapseExist(0, i, 0, j))
						NeuralNetEditor.setWeightSynapse(net, 0, i, 0, j, sum[0] / nc);
					else
						NeuralNetEditor.setWeightSynapse(net, 0, j, 0, i, sum[0] / nc);
					sum[0] = 0;
				}
			});
		});
		if (logging)
			Log.info("Training ended.");
		return true;
	}

	private static void countDelta(BaseNeuron n, double output)
	{
		n.setDelta((output - n.created()) * MathUtils.countDerivative(n));
		n.setDeltaFormed(true);
	}

	private static void countDelta(BaseNeuron n)
	{
		// Log.debug("Counting delta for neuron " + n.getLayerNID() + " in layer " +
		// n.getLayerID());
		double gs = gettersSum(n, true);
		if (gs != -1)
		{
			n.setDelta(MathUtils.countDerivative(n) * gs);
			n.setDeltaFormed(true);
		}
	}

	private static double gettersSum(BaseNeuron n, boolean changeWeights)
	{
		ArrayList<BaseNeuron> al = n.getters();
		sum = 0;

		Parallel.forEach(al, false, (g) -> {
			if (g.deltaFormed() && !breaked)
			{
				sum += net.getWeightSynapse(n, g) * g.delta();
				if (changeWeights)
					changeWeight(n, g);
			} else
			{
				Log.debug("!!!!");
				sum = -1;
				breaked = true;
			}
		});
		/*
		 * for (BaseNeuron g : al) { if (g.deltaFormed()) { sum +=
		 * net.getWeightSynapse(n, g) * g.delta(); if (changeWeights) changeWeight(n,
		 * g); } else { Log.debug("!!!!"); return -1; } }
		 */
		// Log.debug("Counting delta success");
		return sum;
	}

	private static void changeWeight(BaseNeuron n1, BaseNeuron n2)
	{
		double GRAD = n2.delta() * n1.created();
		//System.out.println("Gradient from neuron " + n1.getLayerID() + " " + n1.getLayerNID() + " to " + n2.getLayerID() + " " + n2.getLayerNID() + " = " + GRAD);
		double dW;
		/*
		 * if (n.ID == g.ID && n.type == NeuronType.Reccurent) { for (int i =
		 * net.mem.MemSize() - 1; i > 1; i--) { GRAD += net.mem.peek(i).neuron.delta *
		 * net.mem.peek(i - 1).neuron.createdValue; } GRAD =
		 * ActivatLognType.engineActivatLogn(ActivatLognType.HyperbolicTangent, GRAD,
		 * 1); }
		 */
		try
		{
			dW = E * GRAD + a * prWe.get(n1, n2);
		} catch (NullPointerException e)
		{
			dW = E * GRAD;
			prWe.put(n1, n2, dW);
		}
		// System.out.println("dw from " + n.ID + " to " + g.ID + " = " + dW);
		NeuralNetEditor.changeWeightSynapse(net, n1, n2, dW);
	}
}
