package yocom.janne.traintoolkit;

import yocom.janne.base.BaseNeuralNetwork;

public class NetError
{
	public static double MSE(BaseNeuralNetwork net, double[] idealEpoch, double[] actualEpoch)
	{
		double num = 0;
		for (int i = 0; i < idealEpoch.length; i++)
		{
			num += (idealEpoch[i] - actualEpoch[i]) * (idealEpoch[i] - actualEpoch[i]);
		}
		return num / actualEpoch.length;
	}
}
