import java.util.ArrayList;

import yocom.janne.base.BaseLayer;
import yocom.janne.base.NeuralNetCreator;
import yocom.janne.base.NeuralNetExecutor;
import yocom.janne.base.NeuralNetProgram;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.traintoolkit.Epoch;
import yocom.janne.traintoolkit.NetTrainer;
import yocom.janne.traintoolkit.Set;
import yocom.janne.types.ActivationType;
import yocom.janne.types.NeuralGen;

public class DebugTrain {
	public static void main(String[] args) {
		ArrayList<BaseLayer> layers = new ArrayList<BaseLayer>();
		layers.add(BaseLayer.create(2, ActivationType.ReLU, NeuralGen.Default));
		layers.add(BaseLayer.create(4, ActivationType.ReLU, NeuralGen.Default));
		//layers.add(BaseLayer.create(4, ActivationType.ReLU, NeuralGen.Default));
		layers.add(BaseLayer.create(4, ActivationType.ReLU, NeuralGen.Default));
		layers.add(BaseLayer.create(1, ActivationType.Sigmoid, NeuralGen.Default));
		DefNeuralNetwork dnn = NeuralNetCreator.createDefPerceptron(layers, false);
		NeuralNetProgram prog = NeuralNetCreator.getProgramForPerceptron(dnn);
		Epoch e = new Epoch();
		Set s = new Set();
		s.addToInput(0.2, 0.2);
		s.addToOutput(0.4);
		e.add(s);
		s = new Set();
		s.addToInput(0.3, 0.4);
		s.addToOutput(0.7);
		e.add(s);
		NetTrainer.enableCrashLog();
		NetTrainer.trainPerceptron(dnn, e, 0.2, 0.4, 0.0000000001, 50000, 30);
	
		Log.printArray(" ", NeuralNetExecutor.run(dnn, prog, e.get(0).getInputs()));
		Log.printArray(" ", NeuralNetExecutor.run(dnn, prog, e.get(1).getInputs()));
	}
}
