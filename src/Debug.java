import java.util.ArrayList;

import yocom.janne.base.BaseLayer;
import yocom.janne.base.NeuralNetCreator;
import yocom.janne.base.NeuralNetExecutor;
import yocom.janne.base.NeuralNetProgram;
import yocom.janne.io.Log;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.traintoolkit.Epoch;
import yocom.janne.traintoolkit.NetTrainer;
import yocom.janne.traintoolkit.Set;
import yocom.janne.types.ActivationType;
import yocom.janne.types.NeuralGen;
import yocom.janne.types.NeuronType;

public class Debug
{
	public static void main(String... strings) throws Throwable
	{
		ArrayList<BaseLayer> layers = new ArrayList<BaseLayer>();
		layers.add(BaseLayer.create(2, ActivationType.HyperbolicTangent, NeuronType.Unknown, NeuralGen.Default));
		layers.add(BaseLayer.create(4, ActivationType.HyperbolicTangent, NeuronType.Unknown, NeuralGen.Default));
		layers.add(BaseLayer.create(4, ActivationType.HyperbolicTangent, NeuronType.Unknown, NeuralGen.Default));
		layers.add(BaseLayer.create(1, ActivationType.HyperbolicTangent, NeuronType.Unknown, NeuralGen.Default));

		DefNeuralNetwork net = NeuralNetCreator.createDefPerceptron(layers, false);
		NeuralNetProgram prog = NeuralNetCreator.getProgramForPerceptron(net);

		Epoch e = new Epoch();
		e.add(new Set().addToInput(0.7D, 0.3D).addToOutput(1));
		e.add(new Set().addToInput(0.5D, 0.5D).addToOutput(1));
		e.add(new Set().addToInput(0.1D, 0.9D).addToOutput(1));
		e.add(new Set().addToInput(0.6D, 0.4D).addToOutput(1));

		NetTrainer.trainPerceptron(net, e, 0.2, 0.1, 0.000000000001, 500000, 30);

		NeuralNetExecutor.setLogging(false);

		Log.printArray(" ", NeuralNetExecutor.run(net, prog, 0.7D, 0.3D));
		Log.printArray(" ", NeuralNetExecutor.run(net, prog, 0.5D, 0.5D));
		Log.printArray(" ", NeuralNetExecutor.run(net, prog, 0.1D, 0.9D));
		Log.printArray(" ", NeuralNetExecutor.run(net, prog, 0.6D, 0.4D));
	}
}
