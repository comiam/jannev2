import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferUShort;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import yocom.janne.base.NeuralNetCreator;
import yocom.janne.base.NeuralNetExecutor;
import yocom.janne.base.NeuralNetProgram;
import yocom.janne.network.DefNeuralNetwork;
import yocom.janne.traintoolkit.NetTrainer;
import yocom.janne.types.ActivationType;

public class TrainHopfield
{
	public static void main(String[] args) throws IOException
	{		
		DefNeuralNetwork net = NeuralNetCreator.createHopfieldNN(1600);
		NeuralNetProgram prog = NeuralNetCreator.getProgramForHopfieldNN(net);
		
		BufferedImage img = ImageIO.read(new File("smile.png"));
		NetTrainer.trainHopfieldNN(net, new double[][] {toData(img)});
		
		img = ImageIO.read(new File("damaged2.png"));
		double[] output = NeuralNetExecutor.run(net, prog, toData(img));
		
		ImageIO.write(toImage(fromData(output), img.getWidth(), img.getHeight()), "png", new File("out.png"));
	}
	
	public static int[] fromData(double[] data)
	{
		int[] dataI = new int[data.length];
		for (int i = 0; i < data.length; i++)
		{
			dataI[i] = data[i] == -1 ? -1 : 0;
		}
		return dataI;
	}

	public static double[] toData(BufferedImage img) throws IOException
	{
		int[] data = toInt(img);
		double[] convData = new double[data.length];

		for (int i = 0; i < convData.length; i++)
		{
			convData[i] = ActivationType.engine(ActivationType.Threshold, data[i], 0);
		}
		return convData;
	}

	public static int[] toInt(BufferedImage img) throws IOException
	{
		switch (img.getType())
		{
			case BufferedImage.TYPE_4BYTE_ABGR:
				final byte[] bb = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
				int[] arrayIB = new int[bb.length];
				for (int i = 0; i < arrayIB.length; i++)
				{
					arrayIB[i] = bb[i];
				}
				return arrayIB;
			case BufferedImage.TYPE_USHORT_GRAY:
				final short[] sb = ((DataBufferUShort) img.getRaster().getDataBuffer()).getData();
				int[] arrayIS = new int[sb.length];
				for (int i = 0; i < arrayIS.length; i++)
				{
					arrayIS[i] = sb[i];
				}
				return arrayIS;
			case BufferedImage.TYPE_INT_ARGB:
				
				return ((DataBufferInt) img.getRaster().getDataBuffer()).getData();
			default:
				return null;
		}
	}

	public static BufferedImage toImage(int[] data, int width, int height)
	{
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
		for (int j = 0; j < data.length; j++)
		{
			((DataBufferByte) image.getRaster().getDataBuffer()).setElemDouble(j, data[j]);
		}

		return image;
	}

}
